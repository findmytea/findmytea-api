﻿using AutoMapper;
using FindMyTeaApi.Mapping;
using Xunit;

namespace FindMyTeaApiTest.Mapping
{
    public class AutoMappingConfigurationTest
    {
        [Fact(DisplayName = "Test Mappings")]
        public void TestMapping()
        {
            var profile = new AutoMappingConfiguration();
            var config = new MapperConfiguration(cfg => cfg.AddProfile(profile));
            config.AssertConfigurationIsValid();
        }
    }
}
