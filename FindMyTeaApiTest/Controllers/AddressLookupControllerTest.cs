using System.Threading.Tasks;
using Xunit;


namespace FindMyTeaApiTest.Controllers
{
    public class AddressLookupControllerTest : IClassFixture<ControllerFixture>
    {
        private readonly ControllerFixture _fixture;

        public AddressLookupControllerTest(ControllerFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact(Skip = "Doesn't work at the moment")]
        public async Task ReturnsAddressForPostcode()
        {
            var response = await _fixture.Client.GetAsync("/AddressLookup?postcode=NR4+7AN");

            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            Assert.Equal("Welcome to the Find My Tea API", content);
        }
    }
}
