using System;
using System.Net.Http;
using FindMyTeaApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace FindMyTeaApiTest.Controllers
{    
    public class ControllerFixture : IDisposable
    {
        public TestServer TestServer { get; private set; }

        public HttpClient Client { get { return TestServer.CreateClient(); } }

        public ControllerFixture()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
            TestServer = new TestServer(new WebHostBuilder().UseStartup<Startup>());            
        }

        public void Dispose()
        {
            TestServer.Dispose();
        }
    }
}