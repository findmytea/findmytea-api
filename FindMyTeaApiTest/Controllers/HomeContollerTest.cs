using System.Threading.Tasks;
using Xunit;


namespace FindMyTeaApiTest.Controllers
{
    public class HomeControllerTest : IClassFixture<ControllerFixture>
    {
        private readonly ControllerFixture _fixture;

        public HomeControllerTest(ControllerFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact(Skip = "Takes too long to timeout.")]
        public async Task HomePageReturnsMessage()
        {
            var response = await _fixture.Client.GetAsync("/");

            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            Assert.Equal("Welcome to the Find My Tea API", content);
        }
    }
}
