
using System.IO;
using System.Text;
using System.Threading.Tasks;
using FindMyTeaDal.Repositories;
using Xunit;
using Moq;
using Microsoft.Extensions.Configuration;
using FindMyTea.Image;

namespace FindMyTea.Brand
{
    public class TwitterBrandImportTest
    {
        [Fact(Skip = "Needs some work to mock twitter call.")]
        public async Task ImportFromCSV()
        {
            var brandRepo = new Mock<IBrandRepository>();
            var imageUploader = new Mock<IImageUploader>();
            
            var importer = new TwitterBrandImport(brandRepo.Object, imageUploader.Object);
            await importer.Import(FileStream(), "user", "id");            
        }

        private StreamReader FileStream()
        {
            byte[] byteArray = Encoding.ASCII.GetBytes( CSV );
            var stream = new MemoryStream( byteArray );
            return new StreamReader( stream );
        }


        private const string CSV = @"Name,Description,TwitterHandle,Email,Phone,InstagramHandle,FacebookPageName,Website\n
                                     ,,Lipton,test@test.com,07000000000,,,https://www.lipton.com/gb/home.html\n
                                     ,,Twist_Teas,,,,,https://www.twist-teas.co.uk/\n";
    }
}