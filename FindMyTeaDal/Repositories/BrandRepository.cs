﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaDal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FindMyTeaDal.Repositories
{
    public class BrandRepository : RepositoryBase, IBrandRepository
    {
        private readonly FindMyTeaContext _context;

        public BrandRepository(FindMyTeaContext context, ILogger<BrandRepository> logger) : base(logger)
        {
            _context = context;
        }
         private IQueryable<Entities.Brand> QueryBrandDetails
        {
            get
            {
                return _context.Brands
                .Include(e => e.Image)
                .Include(e => e.BrandImages)
                .ThenInclude(b => b.Image)
                .AsNoTracking()
                .AsQueryable();
            }
        }

        public async Task<IEnumerable<Brand>> QueryAllBrandsAsync()
          => await QueryBrandDetails.OrderBy(o => o.Name).ToListAsync();

        public Task<Brand> QueryBrandByIdAsync(Guid id)
            => QueryBrandDetails.FirstOrDefaultAsync(b => b.Id == id);                 

        public Task<Brand> QueryBrandByTwitterIdAsync(long id)
            => QueryBrandDetails.FirstOrDefaultAsync(b => b.TwitterId == id);        

         public Task<Brand> QueryBrandByNameIdAsync(string name)
            => QueryBrandDetails.FirstOrDefaultAsync(b => b.Name == name);        

        public async Task DeleteBrandAsync(Guid id)
        {
            var brand = await QueryBrandByIdAsync(id);

            await _context.BrandImages.Where(l => l.BrandId == brand.Id).ForEachAsync(item =>
            {
                if (!brand.BrandImages.Any(i => i.ImageId == item.ImageId))
                {
                    _context.BrandImages.Remove(item);
                }

                if (brand.BrandImages.Any(i => i.ImageId == item.ImageId))
                {
                    brand.BrandImages.Remove(brand.BrandImages.First(i => i.ImageId == item.ImageId));
                }
            });

            _context.Brands.Remove(brand);
            await _context.SaveChangesAsync();
        }

        public Task CreateOrUpdateAsync(Brand brand)
        {
            if (brand is null)
            {
                throw new ArgumentNullException(nameof(brand));
            }

            return CreateOrUpdateInternalAsync(brand);
        }

        private async Task CreateOrUpdateInternalAsync(Brand brand)
        {
            if (brand.Image?.Id != null)
            {
                var image = _context.Images.Find(brand.Image.Id);
                _context.Entry(image).State = EntityState.Detached;
                brand.Image = image;
            }

            if (brand.Id != null)
            {
                await _context.BrandImages.Where(l => l.BrandId == brand.Id).ForEachAsync(item =>
                {
                    if (!brand.BrandImages.Any(i => i.ImageId == item.ImageId))
                    {
                        _context.BrandImages.Remove(item);
                    }

                    if (brand.BrandImages.Any(i => i.ImageId == item.ImageId))
                    {
                        brand.BrandImages.Remove(brand.BrandImages.First(i => i.ImageId == item.ImageId));
                    }
                });

                _context.Brands.Update(brand);
            }
            else
            {
                _context.Brands.Add(brand);
            }

            await _context.SaveChangesAsync();
        }
    }
}
