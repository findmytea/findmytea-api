﻿using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Auth;
using System;
using System.Threading.Tasks;

namespace FindMyTeaDal.Repositories
{
    public interface IApiKeyRepository
    {
        public Task<ApiKey> Lookup(Guid key);
    }
}
