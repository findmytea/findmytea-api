
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaDal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FindMyTeaDal.Repositories
{
    public class ImageRepository : RepositoryBase, IImageRepository
    {
        private readonly FindMyTeaContext _context;

        public ImageRepository(FindMyTeaContext context, ILogger<ImageRepository> logger) : base(logger)
        {
            _context = context;
        }

        public async Task<IEnumerable<Image>> QueryImagesAsync()
            => await _context.Images.OrderBy(i => i.OriginalFileName).AsNoTracking().ToArrayAsync();

       public Task CreateOrUpdateAsync(Image image)
       {
            if (image is null)
            {
                throw new ArgumentNullException(nameof(image));
            }

            return CreateOrUpdateInternalAsync(image);
        }

        private async Task CreateOrUpdateInternalAsync(Image image)
        {
            if (image.Id != null)
            {
                _context.Images.Update(image);
            }
            else
            {
                _context.Images.Add(image);
            }

            await _context.SaveChangesAsync();    
        }
    }
}