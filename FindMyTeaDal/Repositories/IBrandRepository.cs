﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaDal.Entities;

namespace FindMyTeaDal.Repositories
{
    public interface IBrandRepository
    {
        Task<IEnumerable<Brand>> QueryAllBrandsAsync();

        Task<Brand> QueryBrandByIdAsync(Guid id);

        Task<Brand> QueryBrandByNameIdAsync(string name);
        
        Task<Brand> QueryBrandByTwitterIdAsync(long id);
        
        Task DeleteBrandAsync(Guid id);

        Task CreateOrUpdateAsync(Brand brand);
    }
}
