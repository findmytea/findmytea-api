﻿using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindMyTeaDal.Repositories
{
    public class ApiKeyRepository : RepositoryBase, IApiKeyRepository
    {
        private readonly FindMyTeaContext _context;

        public ApiKeyRepository(FindMyTeaContext context, ILogger<ApiKeyRepository> logger)
            : base(logger)
        {
            _context = context;
        }

        public async Task<ApiKey> Lookup(Guid key)
        {
            return await _context.ApiKeys.Include(a => a.ApiKeyApiKeyRoles).ThenInclude(a => a.Role).FirstOrDefaultAsync(a => a.Id == key);
        }
    }
}
