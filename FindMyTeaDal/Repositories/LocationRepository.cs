﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;

namespace FindMyTeaDal.Repositories
{
    public class LocationRepository : RepositoryBase, ILocationRepository
    {
        private readonly FindMyTeaContext _context;
        public LocationRepository(FindMyTeaContext context, ILogger<LocationRepository> logger) : base(logger)
        {
            _context = context;
        }

        private IQueryable<Entities.Location> QueryLocationDetails
        {
            get
            {
                return _context.Locations
                .Include(e => e.Image)
                .Include(e => e.BrandLocations)
                .ThenInclude(b => b.Brand)
                .ThenInclude(i => i.Image)
                .Include(e => e.LocationImages)
                .ThenInclude(b => b.Image)
                .AsNoTracking()
                .AsQueryable();
            }
        }

         public async Task<IEnumerable<Entities.Location>> QueryAllLocationsAsync()
             => await QueryLocationDetails.OrderBy(o => o.Name).ToListAsync();

        public async Task<IEnumerable<Entities.Location>> QueryAllLocationsByDistanceFromAsync(Point point, long distance)
            => await QueryLocationDetails.Where(l => l.GeoLocation.Distance(point) < distance).Include(e => e.BrandLocations).OrderBy(l => l.GeoLocation.Distance(point)).ToListAsync();

        public Task<Entities.Location> QueryLocationByIdAsync(Guid id)
            => QueryLocationDetails.FirstOrDefaultAsync(l => l.Id == id);       
            
        public async Task DeleteLocationAsync(Guid id)
        {
            var location = await QueryLocationByIdAsync(id);
            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();           
        }

        public Task CreateOrUpdateAsync(Entities.Location location)
        {
            if (location is null)
            {
                throw new ArgumentNullException(nameof(location));
            }

            return CreateOrUpdateInternalAsync(location);
        }

        private async Task CreateOrUpdateInternalAsync(Entities.Location location)
        {
            if (location.Image?.Id != null)
            {                
                var image = _context.Images.Find(location.Image.Id);
                _context.Entry(image).State = EntityState.Detached;
                location.Image = image;
            }

            if (location.Id != null)
            {
                await _context.LocationBrands.Where(l => l.LocationId == location.Id).ForEachAsync(item =>
                {
                    if (!location.BrandLocations.Any(b => b.BrandId == item.BrandId))
                    {
                        _context.LocationBrands.Remove(item);
                    }

                    if (location.BrandLocations.Any(i => i.BrandId == item.BrandId))
                    {
                        location.BrandLocations.Remove(location.BrandLocations.First(i => i.BrandId == item.BrandId));
                    }
                });                

                await _context.LocationImages.Where(l => l.LocationId == location.Id).ForEachAsync(item =>
                {
                    if (!location.LocationImages.Any(i => i.ImageId == item.ImageId))
                    {
                        _context.LocationImages.Remove(item);
                    }

                    if (location.LocationImages.Any(i => i.ImageId == item.ImageId))
                    {
                        location.LocationImages.Remove(location.LocationImages.First(i => i.ImageId == item.ImageId));
                    }
                });  
                
                _context.Locations.Update(location);
            }
            else
            {
                _context.Locations.Add(location);
            }

            await _context.SaveChangesAsync();
        }
    }
}
