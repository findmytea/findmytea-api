﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Contrib.WaitAndRetry;


namespace FindMyTeaDal
{
    public class RepositoryBase
    {
        private static readonly IAsyncPolicy _transientErrorHandlingPolicy = Policy
            .TimeoutAsync(timeout: TimeSpan.FromSeconds(15))
            .WrapAsync(
                Policy.Handle<TimeoutException>()
                    .WaitAndRetryAsync(
                        sleepDurations: Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromSeconds(1), retryCount: 10),
                        (exception, timeSpan, attempt, context) =>
                        {
                            const string msg = "Retrying database connection for {Attempt} at {Operation}. Waiting {WaitTime}.";
                            var logger = context.GetLogger();
                            logger?.LogWarning(exception, msg, attempt, context.OperationKey, timeSpan);
                        }));

        private readonly ILogger _logger;

        public RepositoryBase(ILogger logger)
        {
            _logger = logger;
        }

        protected async Task<TResult> WithPolicyAsync<TResult>(string operation, Func<Task<TResult>> action)
        {
            var context = new Context(operation).WithLogger(_logger);
            return await _transientErrorHandlingPolicy.ExecuteAsync(_ => action(), context);
        }

        protected async Task WithPolicyAsync(string operation, Func<Task> action)
        {
            var context = new Context(operation).WithLogger(_logger);
            await _transientErrorHandlingPolicy.ExecuteAsync(_ => action(), context);
        }

        protected IQueryable<T> PageQuery<T>(QueryPaged query, IQueryable<T> result)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            if (query.Page <= 0)
            {
                query.Page = 1;
            }

            return result.Skip((query.Page - 1) * query.Limit).Take(query.Limit);
        }
    }
}
