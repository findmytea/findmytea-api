﻿using System;
using Microsoft.Extensions.Logging;
using Polly;

namespace FindMyTeaDal
{
    internal static class PollyContextExtensions
    {
        private const string _loggerKey = "__LoggerKey__";

        public static Context WithLogger(this Context context, ILogger logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            context[_loggerKey] = logger;
            return context;
        }

        public static ILogger GetLogger(this Context context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.TryGetValue(_loggerKey, out var logger)
                ? logger as ILogger
                : null;
        }
    }
}
