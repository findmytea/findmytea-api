﻿namespace FindMyTeaDal
{
    public abstract class QueryPaged
    {
        public int Page { get; set; }

        public int Limit { get; set; }
    }
}
