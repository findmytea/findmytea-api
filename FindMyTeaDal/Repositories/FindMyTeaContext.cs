﻿
using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Auth;
using FindMyTeaDal.Entities.Configuration;
using FindMyTeaDal.Entities.Link;
using FindMyTeaDal.Entities.Metrics;
using Microsoft.EntityFrameworkCore;

namespace FindMyTeaDal
{
    public class FindMyTeaContext : DbContext
    {
        public FindMyTeaContext() { }

        public FindMyTeaContext(DbContextOptions<FindMyTeaContext> options) : base(options) { }

        public DbSet<ApiKey> ApiKeys { get; set; }

        public DbSet<Brand> Brands { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<LocationBrand> LocationBrands { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<LocationImage> LocationImages { get; set; } 

        public DbSet<BrandImage> BrandImages { get; set; }

        public DbSet<Entities.Metrics.Search> MetricsSearch { get; set; }

        public DbSet<SearchPerDay> SearchPerDays { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FindMyTeaDal.Entities.Configuration.Auth.ApiKeyConfiguration());
            modelBuilder.ApplyConfiguration(new FindMyTeaDal.Entities.Configuration.Auth.ApiKeyRoleConfiguration());
            modelBuilder.ApplyConfiguration(new FindMyTeaDal.Entities.Configuration.Auth.ApiKeyApiKeyRoleConfiguration());
            modelBuilder.ApplyConfiguration(new BrandConfiguration());
            modelBuilder.ApplyConfiguration(new LocationConfiguration());
            modelBuilder.ApplyConfiguration(new LocationBrandConfiguration());
            modelBuilder.ApplyConfiguration(new LocationImageConfiguration());
            modelBuilder.ApplyConfiguration(new BrandImageConfiguration());
            modelBuilder.ApplyConfiguration(new ImageConfiguration());
            modelBuilder.ApplyConfiguration(new Entities.Configuration.Metrics.SearchConfiguration());     
            modelBuilder.ApplyConfiguration(new Entities.Configuration.Metrics.SearchPerDayConfiguration());                 
        }
    }
}
