using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaDal.Entities.Metrics;

namespace FindMyTeaDal.Repositories
{
    public interface IMetricsRepository
    {
        Task AddSearchAsync(Search search);

        Task<IEnumerable<SearchPerDay>> QuerySearch();
    }
}
