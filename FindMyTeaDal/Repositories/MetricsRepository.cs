using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaDal.Entities.Metrics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace FindMyTeaDal.Repositories
{
    public class MetricsRepository : RepositoryBase, IMetricsRepository
    {
        private readonly FindMyTeaContext _context;

        public MetricsRepository(FindMyTeaContext context, ILogger<LocationRepository> logger) 
            : base(logger)
        {
            _context = context;
        }

        public async Task AddSearchAsync(Search search)
        {
            _context.MetricsSearch.Add(search);
            await _context.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<SearchPerDay>> QuerySearch()
        {
            return await _context.SearchPerDays.ToListAsync();
        }    
    }
}
