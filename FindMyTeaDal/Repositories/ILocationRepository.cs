﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace FindMyTeaDal.Repositories
{
    public interface ILocationRepository
    {
        Task<IEnumerable<Entities.Location>> QueryAllLocationsByDistanceFromAsync(Point point, long distance);

        Task<IEnumerable<Entities.Location>> QueryAllLocationsAsync();

        Task<Entities.Location> QueryLocationByIdAsync(Guid id);

        Task DeleteLocationAsync(Guid id);

        Task CreateOrUpdateAsync(Entities.Location brand);

    }
}
