
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaDal.Entities;

namespace FindMyTeaDal.Repositories
{
    public interface IImageRepository
    {
        Task<IEnumerable<Image>> QueryImagesAsync();
        
        Task CreateOrUpdateAsync(Image image);
    }
}
