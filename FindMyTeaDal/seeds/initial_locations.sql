CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public; -- Run as super user for each database

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public; -- Run as super user for each database

CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; -- Run as super user for each database

insert into "Locations" ("Id", "Name", "Website", "Retailer", "Cafe", "GeoLocation") values (uuid_generate_v1(), 'Cafe Pure', 'https://www.facebook.com/cafepurestgeorges/', false, true, 'POINT(52.6323202 1.2947649)');
insert into "Locations" ("Id", "Name", "Website", "Retailer", "Cafe", "GeoLocation") values (uuid_generate_v1(), 'Jarrold', 'https://www.jarrold.co.uk/', true, false, 'POINT(52.6294342 1.2936336)');
insert into "Locations" ("Id", "Name", "Website", "Retailer", "Cafe", "GeoLocation") values (uuid_generate_v1(), 'Marzano Cafe & Bar', 'https://www.facebook.com/cafebarmarzano', false, true, 'POINT(52.6277909 1.2909079)');
insert into "Locations" ("Id", "Name", "Website", "Retailer", "Cafe", "GeoLocation") values (uuid_generate_v1(), 'Warings Cafe', 'https://www.waringsathome.co.uk/news/cafemenu/', false, true, 'POINT(52.6260535 1.2952051)');
insert into "Locations" ("Id", "Name", "Website", "Retailer", "Cafe", "GeoLocation") values (uuid_generate_v1(), 'Past And Present Vintage Tearoom', 'https://www.facebook.com/pages/category/Tea-Room/Past-Present-vintage-tearooms-172734643226470/', false, true, 'POINT(52.4896015 1.2342623)');

INSERT INTO "Brands" ("Id", "Name", "Description", "ImageUrl", "SmallImageUrl", "TwitterHandle", "TwitterId", "Website") VALUES(uuid_generate_v1(), 'Teapigs', 'We only use the very best quality whole leaf teas, herbs & spices. B Corp Certified & first tea company to be certified plastic free by A Plastic Planet!', 'http://pbs.twimg.com/profile_images/695200647562555392/CWeD2fBk.jpg', 'http://pbs.twimg.com/profile_images/695200647562555392/CWeD2fBk_normal.jpg', 'teapigs', 37914450, 'https://t.co/KRI5IPkxCv');

insert into "LocationBrand" ("LocationId", "BrandId")
values ((select "Id" from "Locations" where "Name" = 'Jarrold'),(select "Id" from "Brands" where "Name" = 'Teapigs'));

insert into "LocationBrand" ("LocationId", "BrandId")
values ((select "Id" from "Locations" where "Name" = 'Cafe Pure'),(select "Id" from "Brands" where "Name" = 'Teapigs'));

insert into "LocationBrand" ("LocationId", "BrandId")
values ((select "Id" from "Locations" where "Name" = 'Warings Cafe'),(select "Id" from "Brands" where "Name" = 'Teapigs'));

 