﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class LocationFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddressLine1",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Locations",
                nullable: true,
                defaultValue: "UK");

            migrationBuilder.AddColumn<string>(
                name: "County",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreateUsername",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Locations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Postcode",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmallImageUrl",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Town",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwitterHandle",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TwitterId",
                table: "Locations",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUsername",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Locations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedUserTokenId",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "createdUserTokenId",
                table: "Locations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressLine1",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "County",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "CreateUsername",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Created",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Postcode",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "SmallImageUrl",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Town",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "TwitterHandle",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "TwitterId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "UpdateUsername",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "UpdatedUserTokenId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "createdUserTokenId",
                table: "Locations");
        }
    }
}
