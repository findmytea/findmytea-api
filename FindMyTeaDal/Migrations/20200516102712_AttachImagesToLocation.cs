﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class AttachImagesToLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssociateId",
                table: "Images");

            migrationBuilder.RenameColumn(
                name: "createdUserTokenId",
                table: "Locations",
                newName: "CreatedUserTokenId");

            migrationBuilder.CreateTable(
                name: "LocationImage",
                columns: table => new
                {
                    ImageId = table.Column<Guid>(nullable: false),
                    LocationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationImage", x => new { x.LocationId, x.ImageId });
                    table.ForeignKey(
                        name: "FK_LocationImage_Images_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LocationImage_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LocationImage_ImageId",
                table: "LocationImage",
                column: "ImageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LocationImage");

            migrationBuilder.RenameColumn(
                name: "CreatedUserTokenId",
                table: "Locations",
                newName: "createdUserTokenId");

            migrationBuilder.AddColumn<Guid>(
                name: "AssociateId",
                table: "Images",
                type: "uuid",
                nullable: true);
        }
    }
}
