﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class searchesPerDay : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string script =
                @"
                CREATE VIEW ""metrics"".""SearchPerDay""
                AS SELECT date(""DateTime"") as ""DateTime"", count(""DateTime"") as ""NumberOfSearches"" 
                    FROM metrics.""search"" 
                    GROUP BY date(""DateTime"") 
                    ORDER BY ""DateTime"" asc";

            migrationBuilder.Sql(script);
        }

           

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP VIEW ""metrics"".""SearchPerDay""");
        }
    }
}
