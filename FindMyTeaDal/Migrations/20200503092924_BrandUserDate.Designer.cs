﻿// <auto-generated />
using System;
using FindMyTeaDal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NetTopologySuite.Geometries;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FindMyTeaDal.Migrations
{
    [DbContext(typeof(FindMyTeaContext))]
    [Migration("20200503092924_BrandUserDate")]
    partial class BrandUserDate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("FindMyTeaDal.Entities.Brand", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime>("Created")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ImageUrl")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("SmallImageUrl")
                        .HasColumnType("text");

                    b.Property<string>("TwitterHandle")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<long>("TwitterId")
                        .HasColumnType("bigint");

                    b.Property<string>("UserTokenId")
                        .HasColumnType("text");

                    b.Property<string>("Username")
                        .HasColumnType("text");

                    b.Property<string>("Website")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("TwitterId")
                        .IsUnique();

                    b.ToTable("Brands");
                });

            modelBuilder.Entity("FindMyTeaDal.Entities.Link.LocationBrand", b =>
                {
                    b.Property<Guid>("LocationId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("BrandId")
                        .HasColumnType("uuid");

                    b.HasKey("LocationId", "BrandId");

                    b.HasIndex("BrandId");

                    b.ToTable("LocationBrand");
                });

            modelBuilder.Entity("FindMyTeaDal.Entities.Location", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<bool>("Cafe")
                        .HasColumnType("boolean");

                    b.Property<Point>("GeoLocation")
                        .HasColumnType("geography");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<bool>("Retailer")
                        .HasColumnType("boolean");

                    b.Property<string>("Website")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Locations");
                });

            modelBuilder.Entity("FindMyTeaDal.Entities.Link.LocationBrand", b =>
                {
                    b.HasOne("FindMyTeaDal.Entities.Brand", "Brand")
                        .WithMany("BrandLocations")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("FindMyTeaDal.Entities.Location", "Location")
                        .WithMany("BrandLocations")
                        .HasForeignKey("LocationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
