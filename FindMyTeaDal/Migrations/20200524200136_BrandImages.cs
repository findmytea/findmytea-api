﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class BrandImages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ImageId",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateUsername",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Brands",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedUserTokenId",
                table: "Brands",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BrandImage",
                columns: table => new
                {
                    ImageId = table.Column<Guid>(nullable: false),
                    BrandId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandImage", x => new { x.BrandId, x.ImageId });
                    table.ForeignKey(
                        name: "FK_BrandImage_Brands_BrandId",
                        column: x => x.BrandId,
                        principalTable: "Brands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BrandImage_Images_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Brands_ImageId",
                table: "Brands",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_BrandImage_ImageId",
                table: "BrandImage",
                column: "ImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Brands_Images_ImageId",
                table: "Brands",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Brands_Images_ImageId",
                table: "Brands");

            migrationBuilder.DropTable(
                name: "BrandImage");

            migrationBuilder.DropIndex(
                name: "IX_Brands_ImageId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "UpdateUsername",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "UpdatedUserTokenId",
                table: "Brands");
        }
    }
}
