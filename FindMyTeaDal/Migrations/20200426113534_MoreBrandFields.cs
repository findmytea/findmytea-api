﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class MoreBrandFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Twitter",
                table: "Brands");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Brands",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmallImageUrl",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TwitterHandle",
                table: "Brands",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "TwitterId",
                table: "Brands",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Website",
                table: "Brands",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Brands_TwitterId",
                table: "Brands",
                column: "TwitterId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Brands_TwitterId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "SmallImageUrl",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "TwitterHandle",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "TwitterId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Website",
                table: "Brands");

            migrationBuilder.AddColumn<string>(
                name: "Twitter",
                table: "Brands",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
