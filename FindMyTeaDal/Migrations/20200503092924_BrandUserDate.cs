﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class BrandUserDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Website",
                table: "Brands",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Brands",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UserTokenId",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Username",
                table: "Brands",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "UserTokenId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Username",
                table: "Brands");

            migrationBuilder.AlterColumn<string>(
                name: "Website",
                table: "Brands",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
