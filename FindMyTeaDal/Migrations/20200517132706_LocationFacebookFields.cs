﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class LocationFacebookFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "FacebookId",
                table: "Locations",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "FacebookLink",
                table: "Locations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FacebookName",
                table: "Locations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacebookId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "FacebookLink",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "FacebookName",
                table: "Locations");
        }
    }
}
