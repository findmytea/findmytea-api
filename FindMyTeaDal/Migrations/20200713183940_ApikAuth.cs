﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class ApikAuth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "auth");        

            migrationBuilder.CreateTable(
                name: "ApiKeyRoles",
                schema: "auth",
                columns: table => new
                {
                    Role = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiKeyRoles", x => x.Role);
                });

            migrationBuilder.CreateTable(
                name: "ApiKeys",
                schema: "auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiKeys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ApiKeyApiKeyRole",
                schema: "auth",
                columns: table => new
                {
                    ApiKeyId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiKeyApiKeyRole", x => new { x.ApiKeyId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_ApiKeyApiKeyRole_ApiKeys_ApiKeyId",
                        column: x => x.ApiKeyId,
                        principalSchema: "auth",
                        principalTable: "ApiKeys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApiKeyApiKeyRole_ApiKeyRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "auth",
                        principalTable: "ApiKeyRoles",
                        principalColumn: "Role",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApiKeyApiKeyRole_RoleId",
                schema: "auth",
                table: "ApiKeyApiKeyRole",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApiKeyApiKeyRole",
                schema: "auth");

            migrationBuilder.DropTable(
                name: "ApiKeys",
                schema: "auth");

            migrationBuilder.DropTable(
                name: "ApiKeyRoles",
                schema: "auth");         
        }
    }
}
