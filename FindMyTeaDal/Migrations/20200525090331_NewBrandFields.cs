﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FindMyTeaDal.Migrations
{
    public partial class NewBrandFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TwitterHandle",
                table: "Brands",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "FacebookId",
                table: "Brands",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "FacebookLink",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FacebookName",
                table: "Brands",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Brands",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "FacebookId",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "FacebookLink",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "FacebookName",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Brands");

            migrationBuilder.AlterColumn<string>(
                name: "TwitterHandle",
                table: "Brands",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
