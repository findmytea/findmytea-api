﻿using System;

namespace FindMyTeaDal.Entities
{
    public interface IUserAudit
    {
        public string Username { get; set; }

        public string UserTokenId { get; set; }

        public DateTime Created { get; set; }

        public string UpdateUsername { get; set; }

        public string UpdatedUserTokenId { get; set; }

        public DateTime Updated { get; set; }
    }
}
