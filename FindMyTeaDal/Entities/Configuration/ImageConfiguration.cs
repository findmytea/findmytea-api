
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class ImageConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.HasKey(entity => entity.Id);
            builder.Property(entity => entity.Path).IsRequired();
            builder.Property(entity => entity.OriginalFileName).IsRequired();
        }
    }
}
