﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class LocationConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder.HasKey(entity => entity.Id);
            builder.Property(entity => entity.GeoLocation).HasColumnType("geography");

            builder.Property(entity => entity.Name);
            builder.Property(entity => entity.Description);
            builder.Property(entity => entity.AddressLine1);
            builder.Property(entity => entity.AddressLine2);
            builder.Property(entity => entity.Town);
            builder.Property(entity => entity.County);
            builder.Property(entity => entity.Postcode);
            builder.Property(entity => entity.Country).HasDefaultValue("UK");
            builder.Property(entity => entity.Website);
            builder.Property(entity => entity.Email);
            builder.Property(entity => entity.Phone);

            builder.Property(entity => entity.TwitterId);
            builder.Property(entity => entity.TwitterHandle);
            builder.Property(entity => entity.FacebookId);
            builder.Property(entity => entity.FacebookName);
            builder.Property(entity => entity.FacebookLink);
            
            builder.HasOne(entity => entity.Image);

            builder.Property(entity => entity.Retailer);
            builder.Property(entity => entity.Cafe);

            builder.Property(entity => entity.CreateUsername);
            builder.Property(entity => entity.CreatedUserTokenId);
            builder.Property(entity => entity.Created);
            builder.Property(entity => entity.UpdateUsername);
            builder.Property(entity => entity.UpdatedUserTokenId);
            builder.Property(entity => entity.Updated);
        }
    }
}
