﻿
using FindMyTeaDal.Entities.Link;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class LocationBrandConfiguration : IEntityTypeConfiguration<LocationBrand>
    {
        public void Configure(EntityTypeBuilder<LocationBrand> builder)
        {
            builder.HasKey(entity => new { entity.LocationId, entity.BrandId });

            builder.ToTable("LocationBrand");

            builder.HasOne(entity => entity.Location).WithMany(c => c.BrandLocations).HasForeignKey(s => s.LocationId);
            builder.HasOne(entity => entity.Brand).WithMany(c => c.BrandLocations).HasForeignKey(s => s.BrandId);
        }
    }
}
