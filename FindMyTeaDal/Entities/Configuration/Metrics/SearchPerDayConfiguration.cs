using System;
using FindMyTeaDal.Entities.Metrics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration.Metrics
{
    public class SearchPerDayConfiguration : IEntityTypeConfiguration<SearchPerDay>
    {
        public void Configure(EntityTypeBuilder<SearchPerDay> builder)
        {
            if (builder is null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.ToTable("SearchPerDay", "metrics"); 
            builder.HasNoKey();
        }
    }
}
