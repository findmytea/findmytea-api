
using System;
using FindMyTeaDal.Entities.Metrics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration.Metrics
{
    public class SearchConfiguration : IEntityTypeConfiguration<Search>
    {
        public void Configure(EntityTypeBuilder<Search> builder)
        {
            if (builder is null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.ToTable("search", "metrics");
            builder.HasKey(entity => entity.Id);
            builder.Property(entity => entity.NumberOfResults);
            builder.Property(entity => entity.DateTime);
        }
    }
}
