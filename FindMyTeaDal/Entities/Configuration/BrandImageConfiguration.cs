using FindMyTeaDal.Entities.Link;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class BrandImageConfiguration : IEntityTypeConfiguration<BrandImage>
    {
        public void Configure(EntityTypeBuilder<BrandImage> builder)
        {
            builder.HasKey(entity => new { entity.BrandId, entity.ImageId });

            builder.ToTable("BrandImage");

            builder.HasOne(entity => entity.Brand).WithMany(c => c.BrandImages).HasForeignKey(s => s.BrandId);
            builder.HasOne(entity => entity.Image).WithMany(c => c.BrandImages).HasForeignKey(s => s.ImageId);
        }
    }
}

