﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class BrandConfiguration : IEntityTypeConfiguration<Brand>
    {
        public void Configure(EntityTypeBuilder<Brand> builder)
        {
            builder.HasKey(entity => entity.Id);
            builder.Property(entity => entity.Name).HasColumnType("citext").IsRequired();
            builder.HasIndex(entity => entity.Name).IsUnique();
            builder.Property(entity => entity.Description);
            
            builder.Property(entity => entity.Website);
            builder.HasOne(entity => entity.Image);

            builder.Property(entity => entity.Email);
            builder.Property(entity => entity.Phone);
            
            builder.HasIndex(entity => entity.TwitterId).IsUnique();            
            builder.Property(entity => entity.TwitterHandle);
            builder.Property(entity => entity.FacebookId);
            builder.Property(entity => entity.FacebookName);
            builder.Property(entity => entity.FacebookLink);

            builder.Property(entity => entity.Username);
            builder.Property(entity => entity.UserTokenId);
            builder.Property(entity => entity.Created);            

            builder.Property(entity => entity.UpdateUsername);
            builder.Property(entity => entity.UpdatedUserTokenId);
            builder.Property(entity => entity.Updated);
        }
    }
}
