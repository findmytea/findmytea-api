﻿
using FindMyTeaDal.Entities.Link;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FindMyTeaDal.Entities.Configuration
{
    public class LocationImageConfiguration : IEntityTypeConfiguration<LocationImage>
    {
        public void Configure(EntityTypeBuilder<LocationImage> builder)
        {
            builder.HasKey(entity => new { entity.LocationId, entity.ImageId });

            builder.ToTable("LocationImage");

            builder.HasOne(entity => entity.Location).WithMany(c => c.LocationImages).HasForeignKey(s => s.LocationId);
            builder.HasOne(entity => entity.Image).WithMany(c => c.LocationImages).HasForeignKey(s => s.ImageId);
        }
    }
}

