﻿using FindMyTeaDal.Entities.Link.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FindMyTeaDal.Entities.Configuration.Auth
{
    public class ApiKeyApiKeyRoleConfiguration : IEntityTypeConfiguration<ApiKeyApiKeyRole>
    {
        public void Configure(EntityTypeBuilder<ApiKeyApiKeyRole> builder)
        {
            builder.HasKey(entity => new { entity.ApiKeyId, entity.RoleId });

            builder.ToTable("ApiKeyApiKeyRole", "auth");

            builder.HasOne(entity => entity.ApiKey).WithMany(c => c.ApiKeyApiKeyRoles).HasForeignKey(s => s.ApiKeyId);
            builder.HasOne(entity => entity.Role).WithMany(c => c.ApiKeyApiKeyRoles).HasForeignKey(s => s.RoleId);
        }
    }
}
