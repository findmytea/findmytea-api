﻿using FindMyTeaDal.Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FindMyTeaDal.Entities.Configuration.Auth
{
    class ApiKeyRoleConfiguration : IEntityTypeConfiguration<ApiKeyRole>
    {
        public void Configure(EntityTypeBuilder<ApiKeyRole> builder)
        {
            if (builder is null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.ToTable("ApiKeyRoles", "auth");
            builder.HasKey(entity => entity.Role);
        }
    }
}
