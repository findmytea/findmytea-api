﻿using FindMyTeaDal.Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace FindMyTeaDal.Entities.Configuration.Auth
{
    class ApiKeyConfiguration : IEntityTypeConfiguration<ApiKey>
    {
        public void Configure(EntityTypeBuilder<ApiKey> builder)
        {
            if (builder is null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.ToTable("ApiKeys", "auth");
            builder.HasKey(entity => entity.Id);            
        }
    }
}
