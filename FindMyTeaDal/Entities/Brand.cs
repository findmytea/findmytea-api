﻿using System;
using System.Collections.Generic;
using FindMyTeaDal.Entities.Link;

namespace FindMyTeaDal.Entities
{
    public class Brand : IHasImage, IUserAudit
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long TwitterId { get; set; }

        public string TwitterHandle { get; set; }

        public long FacebookId { get; set; }

        public string FacebookName { get; set; }

        public string FacebookLink { get; set; }

        public string Description { get; set; }     

        public string Website { get; set; }

        public string Email  { get; set; }

        public string Phone  { get; set; }

        public Image Image { get; set; }

        public ICollection<BrandImage> BrandImages { get; set; }

        public string Username { get; set; }
        
        public string UserTokenId { get; set; }
        
        public DateTime Created { get; set; }

        public string UpdateUsername { get; set; }
        
        public string UpdatedUserTokenId { get; set; }
        
        public DateTime Updated { get; set; }

        public ICollection<LocationBrand> BrandLocations { get; set; }
    }
}
