using System;
using System.Collections.Generic;
using System.Linq;
using FindMyTeaDal.Entities.Link;
using NetTopologySuite.Geometries;

namespace FindMyTeaDal.Entities
{
    public class Location : IHasImage
    {
        public static readonly string UK = "UK";
        
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public string Country { get; set; }

        public string Website { get; set; }
        
        public string Email  { get; set; }

        public string Phone  { get; set; }

        public long TwitterId { get; set; }

        public string TwitterHandle { get; set; }

        public long FacebookId { get; set; }

        public string FacebookName { get; set; }

        public string FacebookLink { get; set; }

        public Image Image { get; set; }

        public bool Retailer { get; set; }

        public bool Cafe { get; set; }

        public string CreateUsername { get; set; }
        
        public string CreatedUserTokenId { get; set; }
        
        public DateTime Created { get; set; }

        public string UpdateUsername { get; set; }
        
        public string UpdatedUserTokenId { get; set; }
        
        public DateTime Updated { get; set; }

        public Point GeoLocation { get; set; }

        public ICollection<LocationBrand> BrandLocations { get; set; }

        public ICollection<LocationImage> LocationImages { get; set; }

        public Location()
        {}

        public Location(string name,
                        string description,
                        string addressLine1,
                        string addressLine2,
                        string town,
                        string county,                        
                        string postcode,
                        string country,
                        string website,
                        string email,
                        string phone,
                        long twitterId,
                        string twitterHandle,
                        long facebookId,
                        string facebookName,
                        string facebookLink,
                        Image image,
                        bool retailer,
                        bool cafe,
                        Point geoLocation,
                        string createUsername,
                        string createdUsertokenId,
                        DateTime created,
                        string updateUsername,
                        string updatedUsertokenId,
                        DateTime updated,
                        IEnumerable<Brand> brands,
                        IEnumerable<Image> images)
        {
            Name = name;
            Description = description;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Town = town;
            County = county;
            Postcode = postcode;
            Country = country;
            Website = website;
            Phone = phone;
            Email = email;
            TwitterId = twitterId;
            TwitterHandle = twitterHandle;
            FacebookId = facebookId;
            FacebookName = facebookName;
            FacebookLink = facebookLink;
            Image = image;
            Retailer = retailer;
            Cafe = cafe;
            GeoLocation = geoLocation;
            CreateUsername = createUsername;
            CreatedUserTokenId = createdUsertokenId;
            Created = created;
            UpdateUsername = updateUsername;
            UpdatedUserTokenId = updatedUsertokenId;
            Updated = updated;
            BrandLocations = brands.Select(b => new  LocationBrand() { BrandId = b.Id }).ToList();
            LocationImages = images.Select(i => new LocationImage() { ImageId = i.Id }).ToList();
        }
    }
}