﻿
namespace FindMyTeaDal.Entities
{
    public interface IHasImage
    {
        Image Image { get; set; }
    }
}
