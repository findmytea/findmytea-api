﻿using System;

namespace FindMyTeaDal.Entities.Link
{
    public class LocationBrand
    {
        public Guid BrandId { get; set; }
        public Brand Brand { get; set; }
        public Guid LocationId { get; set; }
        public Location Location { get; set; }
    }
}
