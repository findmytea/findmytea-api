using System;

namespace FindMyTeaDal.Entities.Link
{
    public class BrandImage
    {
        public Guid? ImageId { get; set; }

        public Image Image { get; set; }

        public Guid BrandId { get; set; }

        public Brand Brand { get; set; }
    }
}
