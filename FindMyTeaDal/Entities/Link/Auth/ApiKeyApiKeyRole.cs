﻿using FindMyTeaDal.Entities.Auth;
using System;

namespace FindMyTeaDal.Entities.Link.Auth
{
    public class ApiKeyApiKeyRole
    {
        public Guid? ApiKeyId { get; set; }

        public ApiKey ApiKey { get; set; }

        public string RoleId { get; set; }

        public ApiKeyRole Role { get; set; }
    }
}
