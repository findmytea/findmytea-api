﻿using System;

namespace FindMyTeaDal.Entities.Link
{
    public class LocationImage
    {
        public Guid? ImageId { get; set; }

        public Image Image { get; set; }

        public Guid LocationId { get; set; }

        public Location Location { get; set; }
    }
}
