﻿using FindMyTeaDal.Entities.Link.Auth;
using System;
using System.Collections.Generic;
using System.Text;

namespace FindMyTeaDal.Entities.Auth
{
    public class ApiKeyRole
    {
        public string Role { get; set; }

        public IEnumerable<ApiKeyApiKeyRole> ApiKeyApiKeyRoles { get; }
    }
}
