﻿using FindMyTeaDal.Entities.Link.Auth;
using System;
using System.Collections.Generic;
using System.Text;

namespace FindMyTeaDal.Entities.Auth
{
    public class ApiKey
    {
        public Guid Id { get; set; }
        
        public IEnumerable<ApiKeyApiKeyRole> ApiKeyApiKeyRoles { get; }
    }
}
