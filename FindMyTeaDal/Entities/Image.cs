using System;
using System.Collections.Generic;
using FindMyTeaDal.Entities.Link;

namespace FindMyTeaDal.Entities
{
    public class Image
    {
        public Guid? Id { get; set; }

        public string Path { get; set; }

        public string OriginalFileName  { get; set; }

        public string LocalPath { get { return $"{Path}/{Id}_{OriginalFileName}"; } }

        public ICollection<LocationImage> LocationImages { get; set; }

        public ICollection<BrandImage> BrandImages { get; set; }

        public Image()
        {
            Id = null;
        }       
        
        public string FullPath(string basePath)
        {
            return $"{basePath}/{LocalPath}"; 
        }
    }
}
