using System;

namespace FindMyTeaDal.Entities.Metrics
{
    public class Search
    {   
        public Search()
        {
        }

        public Search(long numberOfResults)
        {
            NumberOfResults = numberOfResults;
            DateTime = DateTime.Now;
        }
        public Guid Id { get; set; }

        public long NumberOfResults { get; set; }

        public DateTime DateTime { get; set; }
    }
}
