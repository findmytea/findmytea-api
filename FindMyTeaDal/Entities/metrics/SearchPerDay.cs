using System;

namespace FindMyTeaDal.Entities.Metrics
{
    public class SearchPerDay
    {   
        public long NumberOfSearches { get; set; }

        public DateTime DateTime { get; set; }
    }
}
