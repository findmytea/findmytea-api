FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy everything else and build
COPY . ./
RUN dotnet nuget add source https://api.findmytea.co.uk:9011/api/v3/index.json
RUN dotnet publish FindMyTeaApi.sln -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 
WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 5001
ENTRYPOINT [ "dotnet", "FindMyTeaApi.dll" ]

