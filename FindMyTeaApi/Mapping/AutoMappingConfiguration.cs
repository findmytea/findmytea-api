﻿
using System;
using System.Collections.Generic;
using AutoMapper;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaApi.Models;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Metrics;

namespace FindMyTeaApi.Mapping
{
    public class AutoMappingConfiguration : Profile
    {
        public AutoMappingConfiguration()
        {
            CreateMap<LocationSearchResult, LocationSearchResultViewModel>()
                .ForMember(dest => dest.Image, opt => opt.MapFrom<ImageToImageViewModelResolver<LocationSearchResultViewModel>>());

            CreateMap<Brand, BrandSummaryViewModel>()
                .ForMember(dest => dest.Image, opt => opt.MapFrom<ImageToImageViewModelResolver<BrandSummaryViewModel>>());

            CreateMap<Brand, BrandDetailViewModel>()
                .ForMember(dest => dest.Image, opt => opt.MapFrom<ImageToImageViewModelResolver<BrandDetailViewModel>>())
                .ForMember(dest => dest.Images, opt => opt.MapFrom<BrandImagesToImageViewModelsResolver>());

            CreateMap<Location, LocationViewModel>()
                .ForMember(dest => dest.Image, opt => opt.MapFrom<ImageToImageViewModelResolver<LocationViewModel>>())
                .ForMember(dest => dest.Brands, opt => opt.MapFrom<BrandToBrandSummaryViewModelsResolver<LocationViewModel>>())
                .ForMember(dest => dest.GeoX, opt => opt.MapFrom<GeoXToRexover>())
                .ForMember(dest => dest.GeoY, opt => opt.MapFrom<GeoYToRexover>());

            CreateMap<Location, LocationDetailViewModel>()
                .ForMember(dest => dest.Image, opt => opt.MapFrom<ImageToImageViewModelResolver<LocationDetailViewModel>>())
                .ForMember(dest => dest.Brands, opt => opt.MapFrom<BrandToBrandSummaryViewModelsResolver<LocationDetailViewModel>>())
                .ForMember(dest => dest.Images, opt => opt.MapFrom<LocationImagesToImageViewModelsResolver>())
                .ForMember(dest => dest.GeoX, opt => opt.MapFrom<GeoXToRexover>())
                .ForMember(dest => dest.GeoY, opt => opt.MapFrom<GeoYToRexover>());

            CreateMap<CreateBrand, Brand>()
                .ForMember(dest => dest.BrandLocations, opt => opt.Ignore())
                .ForMember(dest => dest.Updated, opt => opt.MapFrom<SetDateResolver>())
                .ForMember(dest => dest.Created, opt => opt.MapFrom<SetDateResolver>())
                .ForMember(dest => dest.UpdateUsername, opt => opt.MapFrom<SetUserResolover>())
                .ForMember(dest => dest.Username, opt => opt.MapFrom<SetUserResolover>())
                .ForMember(dest => dest.UpdatedUserTokenId, opt => opt.MapFrom<SetUserTokenIdResolver>())
                .ForMember(dest => dest.UserTokenId, opt => opt.MapFrom<SetUserTokenIdResolver>())
                .ForMember(dest => dest.BrandImages, opt => opt.MapFrom<ImagesToBrandImagesResolver>());

            CreateMap<UpdateBrand, Brand>()
                .ForMember(dest => dest.BrandLocations, opt => opt.Ignore())
                .ForMember(dest => dest.Username, opt => opt.Ignore())
                .ForMember(dest => dest.UserTokenId, opt => opt.Ignore())
                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.Updated, opt => opt.MapFrom<SetDateResolver>())
                .ForMember(dest => dest.UpdateUsername, opt => opt.MapFrom<SetUserResolover>())
                .ForMember(dest => dest.UpdatedUserTokenId, opt => opt.MapFrom<SetUserTokenIdResolver>())
                .ForMember(dest => dest.BrandImages, opt => opt.MapFrom<ImagesToBrandImagesResolver>());

            CreateMap<SearchPerDay, SearchPerDayViewModel>();
        }
    }
}
