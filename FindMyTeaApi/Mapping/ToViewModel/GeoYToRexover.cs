﻿using AutoMapper;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Mapping
{
    public class GeoYToRexover : IValueResolver<Location, IGeoViewModel, double>
    {
        public double Resolve(Location source, IGeoViewModel destination, double destMember, ResolutionContext context)
        {
            return source.GeoLocation.Y;
        }
    }
}
