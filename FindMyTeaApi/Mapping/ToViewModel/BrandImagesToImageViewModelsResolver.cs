﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Mapping
{
    public class BrandImagesToImageViewModelsResolver : IValueResolver<Brand, BrandDetailViewModel, IEnumerable<ImageViewModel>>
    {
        public IEnumerable<ImageViewModel> Resolve(Brand source, BrandDetailViewModel destination, IEnumerable<ImageViewModel> destMember, ResolutionContext context)
        {
            return source.BrandImages.Select(i => new ImageViewModel(i.Image, (string)context.Items["imagePath"]));
        }
    }
}