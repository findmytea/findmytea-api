﻿using System;

using AutoMapper;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Mapping
{
    public class ImageToImageViewModelResolver<T> : IValueResolver<IHasImage, T, ImageViewModel>
    {
        public ImageViewModel Resolve(IHasImage source, T destination, ImageViewModel destMember, ResolutionContext context)
        {
            return new ImageViewModel(source.Image, (string) context.Items["imagePath"]);
        }
    }
    
}
