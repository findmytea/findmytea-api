﻿
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Mapping
{
    public class BrandToBrandSummaryViewModelsResolver<T> : IValueResolver<Location, T, IEnumerable<BrandSummaryViewModel>> where T : LocationViewModel
    {
        public IEnumerable<BrandSummaryViewModel> Resolve(Location source, T destination, IEnumerable<BrandSummaryViewModel> destMember, ResolutionContext context)
        {
            return source.BrandLocations.Select(b => context.Mapper.Map<BrandSummaryViewModel>(b.Brand));
        }
    }
}
