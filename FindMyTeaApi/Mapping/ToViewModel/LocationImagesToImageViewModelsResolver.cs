﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FindMyTeaApi.ViewModels;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Mapping
{
    public class LocationImagesToImageViewModelsResolver : IValueResolver<Location, LocationDetailViewModel, IEnumerable<ImageViewModel>>
    {
        public IEnumerable<ImageViewModel> Resolve(Location source, LocationDetailViewModel destination, IEnumerable<ImageViewModel> destMember, ResolutionContext context)
        {
            return source.LocationImages.Select(i => new ImageViewModel(i.Image, (string)context.Items["imagePath"]));
        }
    }
}
