﻿using System;
using AutoMapper;
using FindMyTeaDal.Entities;
using WebLib.Messages;

namespace FindMyTeaApi.Mapping
{
    public class SetDateResolver : IValueResolver<ICommand, IUserAudit, DateTime>
    {
        public DateTime Resolve(ICommand source, IUserAudit destination, DateTime destMember, ResolutionContext context)
        {
            return DateTime.Now;
        }
    }
}
