﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Link;

namespace FindMyTeaApi.Mapping
{
    public class ImagesToBrandImagesResolver : IValueResolver<CreateBrand, Brand, ICollection<BrandImage>>
    {
        public ICollection<BrandImage> Resolve(CreateBrand source, Brand destination, ICollection<BrandImage> destMember, ResolutionContext context)
        {
            return source.Images.Select(i => new BrandImage() { ImageId = i.Id }).ToList(); 
        }
    }
}
