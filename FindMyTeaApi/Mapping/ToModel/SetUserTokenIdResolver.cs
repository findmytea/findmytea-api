﻿using AutoMapper;
using FindMyTeaDal.Entities;
using WebLib.Messages;

namespace FindMyTeaApi.Mapping
{
    public class SetUserTokenIdResolver : IValueResolver<IAuthorizedMessage, IUserAudit, string>
    {
        public string Resolve(IAuthorizedMessage source, IUserAudit destination, string destMember, ResolutionContext context)
        {
            return source?.User?.TokenId;
        }
    }
}
