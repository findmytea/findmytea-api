using System.Collections.Generic;
using FindMyTeaApi.Models;

namespace FindMyTeaApi.ViewModels
{
    public class TwitterUserViewModel
    {
        public long Id { get; private set; }

        public string ScreenName { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public string ImageUrl { get; private set; }

        public string SmallImageUrl { get; private set; }

        public string Website { get; set; }

        public TwitterUserViewModel(TwitterUser user)
        {
            Id = user.Id;
            ScreenName = user.ScreenName;
            Name = user.Name;
            Description = user.Description;
            ImageUrl = user.ImageUrl;
            SmallImageUrl = user.SmallImageUrl;
            Website = user.Website;
        }
    }
}
