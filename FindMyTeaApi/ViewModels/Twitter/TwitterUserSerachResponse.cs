using System.Collections.Generic;
using System.Linq;

namespace FindMyTeaApi.ViewModels
{
    public class TwitterUserSerachResponse
    {
        public IEnumerable<TwitterUserViewModel> Users { get; private set; }

        public TwitterUserSerachResponse(IEnumerable<TwitterUserViewModel> users)
        {
            Users = users.OrderBy(t => t.Name);
        }
    }
}