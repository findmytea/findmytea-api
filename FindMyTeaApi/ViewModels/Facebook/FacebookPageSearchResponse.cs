using System.Collections.Generic;
using System.Linq;

namespace FindMyTeaApi.ViewModels
{
    public class FacebookPageSearchResponse
    {
        public IEnumerable<FacebookPageViewModel> Pages { get; private set; }

        public FacebookPageSearchResponse(IEnumerable<FacebookPageViewModel> pages)
        {
            Pages = pages.OrderBy(p => p.Name);
        }
    }
}