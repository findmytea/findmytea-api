using System.Collections.Generic;
using FindMyTeaApi.Models;

namespace FindMyTeaApi.ViewModels
{
    public class FacebookPageViewModel
    {
        public long Id { get; private set; }

        public string Name { get; private set; }

        public string Link { get; set; }

        public string About {get; set; }

        public FacebookPageViewModel(FacebookPage page)
        {
            Id = page.Id;       
            Name = page.Name;            
            Link = page.Link;
            About = page.About;
        }
    }
}
