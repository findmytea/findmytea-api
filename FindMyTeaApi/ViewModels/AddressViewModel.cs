using System;
using FindMyTeaApi.Models;

namespace FindMyTeaApi.ViewModels
{
    public class AddressViewModel
    {
        public AddressViewModel(Address address)
        {
            if (address is null)
            {
                throw new ArgumentNullException(nameof(address));
            }

            Organisation = address.Organisation;
            Premise = address.Premise;
            Street = address.Street;
            AddressLine1 = address.AddressLine1;
            DependentLocality = address.DependentLocality;
            DoubleDependentLocality = address.DoubleDependentLocality;
            AddressLine2 = address.AddressLine2;
            TownCity = address.TownCity;
            County = address.County;
            Postcode = address.Postcode;
            Latitude = address.Latitude;
            Longitude = address.Longitude;
        }

        public string Organisation { get; private set; }

        public string Premise { get; set; }

        public string Street { get; set; }

        public string AddressLine1 { get; set; }

        public string DependentLocality { get; set; }

        public string DoubleDependentLocality { get; set; }

        public string AddressLine2 { get; private set; }

        public string TownCity { get; private set; }

        public string County { get; private set; }

        public string Postcode { get; private set; }        

        public double Longitude { get; private set;}

        public double Latitude { get; private set;}
    }
}