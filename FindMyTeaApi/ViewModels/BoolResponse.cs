
namespace FindMyTeaApi.ViewModels
{
    public class BoolResponse
    {
        public bool Value { get; private set; }

        public BoolResponse()
        {}

        public BoolResponse(bool value)
        {
            Value = value;
        }
    }
}