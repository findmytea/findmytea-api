using System.Collections.Generic;

namespace FindMyTeaApi.ViewModels
{
    public class BrandResponse
    {
        public IEnumerable<BrandSummaryViewModel> Brands { get; private set; }

        public BrandResponse()
        {}

        public BrandResponse(IEnumerable<BrandSummaryViewModel> brands)
        {
            Brands = brands;
        }
    }
}