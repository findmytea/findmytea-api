﻿
namespace FindMyTeaApi.ViewModels
{
    public interface IGeoViewModel
    {
        double GeoX { get; set; }

        double GeoY { get; set; }
    }
}
