using System;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.ViewModels
{
    public class ImageViewModel
    {
        public Guid? Id { get; set; }

        public Uri Uri { get; set; }        

        public ImageViewModel(Image image, string imageBase)
        {            
            if (image != null)
            {
                Id = image.Id != null ? image.Id : null;
                Uri = new Uri(image.FullPath(imageBase));
            }
        }
    }
}
