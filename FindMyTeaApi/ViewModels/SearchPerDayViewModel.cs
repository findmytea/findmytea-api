using System;

namespace FindMyTeaApi.ViewModels
{
    public class SearchPerDayViewModel
    {
        public long NumberOfSearches { get; set; }

        public DateTime DateTime { get; set; }
    }
}