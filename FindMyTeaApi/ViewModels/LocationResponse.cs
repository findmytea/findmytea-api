using System.Collections.Generic;

namespace FindMyTeaApi.ViewModels
{
    public class LocationResponse<T>
    {
        public IEnumerable<T> Locations { get; private set; }

        public double? GeoX { get; set; }

        public double? GeoY { get; set; }


        public LocationResponse()
        {}
        
        public LocationResponse(IEnumerable<T> locations)
            : this(locations, default, default)
        {}


        public LocationResponse(IEnumerable<T> locations, double? lat, double? lng)
        {
            Locations = locations;
            GeoX = lat;
            GeoY = lng;
        }
    }
}
