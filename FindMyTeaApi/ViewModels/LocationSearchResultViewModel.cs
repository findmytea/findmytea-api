using System;
using System.Collections.Generic;
using System.Linq;
using FindMyTeaApi.Models;


namespace FindMyTeaApi.ViewModels
{
    public class LocationSearchResultViewModel : AbstractLocationViewModel
    {
        public double? Distance { get; private set; }
    }
}
