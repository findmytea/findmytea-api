
using System.Collections.Generic;

namespace FindMyTeaApi.ViewModels
{
    public class BrandDetailViewModel : BrandSummaryViewModel
    {
        public long FacebookId { get; set; }

        public string FacebookName { get; set; }

        public string FacebookLink { get; set; }
        
        public long TwitterId { get; set; }

        public string TwitterHandle { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public IEnumerable<ImageViewModel> Images { get; set; }      
    }
}