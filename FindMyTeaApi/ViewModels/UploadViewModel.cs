using System;

namespace FindMyTeaApi.ViewModels
{
    public class UploadViewModel
    {
        public Uri Uri { get; set; }
    }
}