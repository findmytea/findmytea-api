using System.Collections.Generic;

namespace FindMyTeaApi.ViewModels
{
    public class SearchesPerDayResponse
    {
        public IEnumerable<SearchPerDayViewModel> SearchesPerDay { get; private set; }

        public SearchesPerDayResponse(IEnumerable<SearchPerDayViewModel> searchesPerDay)
        {
            SearchesPerDay = searchesPerDay;
        }
    }
}