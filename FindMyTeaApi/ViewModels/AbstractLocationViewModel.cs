using System;
using System.Collections.Generic;

namespace FindMyTeaApi.ViewModels
{
    public abstract class AbstractLocationViewModel : IGeoViewModel
    {
        public Guid Id { get; protected set; }

        public string Name { get; protected set; }

        public string Website { get; protected set; }

        public bool Retailer { get; protected set; }
        
        public bool Cafe { get; protected set; }        

        public string Description { get; protected set; }

        public string AddressLine1 { get; protected set; }

        public string AddressLine2 { get; protected set; }

        public string Town { get; protected set; }

        public string County { get; protected set; }

        public string Postcode { get; protected set; }

        public string Country { get; protected set; }

        public double GeoX { get; set; }

        public double GeoY { get; set; }

        public ImageViewModel Image { get; set; }

        public IEnumerable<BrandSummaryViewModel> Brands {get; set; }

        protected AbstractLocationViewModel()
        { }      
    }
}
