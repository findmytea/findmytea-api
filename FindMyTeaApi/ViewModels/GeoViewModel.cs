namespace FindMyTeaApi.ViewModels
{
    public class GeoViewModel : IGeoViewModel
    {
        public double GeoX { get; set; }

        public double GeoY { get; set; }
    }
}
