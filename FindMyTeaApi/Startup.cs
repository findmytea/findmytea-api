using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Amazon.S3;

using Microsoft.EntityFrameworkCore;
using FindMyTeaDal.Repositories;
using FindMyTeaDal;
using WebLib.Db;
using WebLib.Geo;
using WebLib.Cache;
using WebLib.MessageBus;
using WebLib.Handlers;
using Microsoft.AspNetCore.Mvc;
using WebLib.Authentication;
using System.Net.Http;
using FindMyTea.Postcoder;
using WebLib.S3;
using AutoMapper;
using FindMyTeaApi.Models;
using Tweetinvi;
using StackExchange.Redis;
using Microsoft.AspNetCore.DataProtection;
using WebLib.Authentication.FindMyIdentity;
using FindMyTeaApi.Lib.Authentication;
using FindMyTeaApi.Lib.Authentication.ApiKey;

namespace FindMyTeaApi
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env, ILogger<Startup> logger)
        {
            Configuration = BuildConfiguration(env);
            CurrentEnvironment = env;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        private IWebHostEnvironment CurrentEnvironment { get; set; }
        
        private ILogger<Startup> _logger;

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureCors(services, Configuration);
            services.ConfigureFindMyIdentity(CurrentEnvironment, Configuration).AddApiKey(options => { });           

            services.ConfigureAuthPolicies();

            services.AddControllers()
                .AddFindMyIdentityActionFilter()
                .ConfigureApiBehaviorOptions(options => {
                options.InvalidModelStateResponseFactory = context =>
                {
                    // TODO: Log bad reqests,
                    return new BadRequestObjectResult(context.ModelState);
                };
            });            

            ConfigureCache(services, CurrentEnvironment, Configuration, _logger);
            ConfigureDb(services, Configuration);

            var keys = new TwitterKeys();
            Configuration.GetSection(TwitterKeys.SectionName).Bind(keys);
            Auth.SetUserCredentials(keys.APIkey, keys.APISecretKey, keys.AccessToken, keys.AccessTokenSecret);

            services.AddDispatchers();
            services.AddAutoMapper(typeof(Startup));
            services.AddHandlers<Startup>();

            services.AddSingleton<IGeoService, GeoService>();
            services.AddSingleton<ICacheService, CacheService>();
            services.AddScoped<HttpClient>();
            services.AddScoped<IPostcoder, Postcoder>();

            services.AddScoped<IAmazonS3Factory, DigitalOceanSpacesS3Factory>();
            services.AddHttpClient<IS3Client, S3Client>();

            services.AddScoped<IApiKeyRepository, ApiKeyRepository>(); 
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IBrandRepository, BrandRepository>();
            services.AddScoped<IImageRepository, ImageRepository>();
            services.AddScoped<IMetricsRepository, MetricsRepository>();           
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, FindMyTeaContext dbContext)
        {
            try
            {
                dbContext.Database.OpenConnection();
                dbContext.Database.Migrate();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Welcome to the Find My Tea API");
                });
            });
        }

        private static IConfigurationRoot BuildConfiguration(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json",
                             optional: false,
                             reloadOnChange: true)
                 .AddJsonFile("appsettings.Development.json",
                             optional: false,
                             reloadOnChange: true)
                .AddEnvironmentVariables();

                if (env.IsDevelopment())
                {
                    builder.AddUserSecrets<Startup>();
                }

            return builder.Build();
        }

        private static void ConfigureDb(IServiceCollection services, IConfiguration config)
        {
            var builder = new PostgreSqlConnectionStringBuilder(config["DATABASE_URL"])
            {
                Pooling = true,
                TrustServerCertificate = true,
                SslMode = SslMode.Prefer
            };

            services.AddDbContext<FindMyTeaContext>(options => options.UseNpgsql(builder.ConnectionString, x => x.UseNetTopologySuite()));
        }

        private static void ConfigureCache(IServiceCollection services, IWebHostEnvironment env, IConfiguration config, ILogger<Startup> _logger)
        {
            var tokens = config["REDIS_URL"].Split(':', '@');
            var connectionString = $"{tokens[3]}:{tokens[4]},password={tokens[2]}";

            try
            {
                var redisKey = env.IsDevelopment() ? "FMT-DataProtection-Keys-Dev" : "FMT-DataProtection-Keys";
                var redis = ConnectionMultiplexer.Connect(connectionString);
                services.AddDataProtection().PersistKeysToStackExchangeRedis(redis, redisKey);
            }
            catch(Exception e)
            {
                _logger.LogWarning($"Failed to PersistKeysToStackExchangeRedis: {e.Message}");
            }

            try
            {
                services.AddStackExchangeRedisCache(options =>
                {
                    options.ConfigurationOptions = ConfigurationOptions.Parse(connectionString);
                });
            }
            catch(Exception e)
            {
                _logger.LogWarning($"Failed to AddStackExchangeRedisCache: {e.Message}");
                _logger.LogWarning("Using memory cache.");
                services.AddDistributedMemoryCache();
            }
        }

        private static void ConfigureCors(IServiceCollection services, IConfiguration config)
        {
            var cors = new string[] { "http://localhost:3000", "https://findmytea.herokuapp.com" };
            services.AddCors(options => options.AddDefaultPolicy(builder => builder.WithOrigins(cors).AllowAnyHeader().AllowAnyMethod().AllowCredentials()));
        }
    }
}
