using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTea.Postcoder;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.Models;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FindMyTea.Handlers.AddressLookup
{
    public class AddressLookupByPostcodeHandler : IQueryHandler<FindMyTeaApi.Messages.Queries.AddressLookup, IEnumerable<Address>>
    {
        private readonly IPostcoder _postcoder;
        private readonly ILogger<AddressLookupByPostcodeHandler> _logger;

        private readonly IWebHostEnvironment _env;

        public AddressLookupByPostcodeHandler(ILogger<AddressLookupByPostcodeHandler> logger, IPostcoder postcoder, IWebHostEnvironment env)
        {
            _postcoder = postcoder;
            _logger = logger;
            _env = env;
        }

        public Task<Result<IEnumerable<Address>>> HandleAsync(FindMyTeaApi.Messages.Queries.AddressLookup query)
        {
            if (query is null)
            {
                throw new System.ArgumentNullException(nameof(query));
            }

            return HandleInternalAsync(query);
        }

        private async Task<Result<IEnumerable<Address>>> HandleInternalAsync(FindMyTeaApi.Messages.Queries.AddressLookup query)
        {
            return new Result<IEnumerable<Address>>(await Addresses(query));
        }

        private async Task<IEnumerable<Address>> Addresses(FindMyTeaApi.Messages.Queries.AddressLookup query)
        {
            if (_env.IsProduction())
            {
                _logger.LogTrace("#HTTP: AddressLookupByPostcodeHandler:HandleAsync");

                var addressReturn = await _postcoder.AddressLookup(query.SearchTerm);

                if (!string.IsNullOrWhiteSpace(addressReturn.Error))
                {
                    _logger.LogError($"Postcoder error: '{addressReturn.Error}' for {query.SearchTerm}");
                }

                return addressReturn.Addresses.Select(a => new Address(a));
            }
            
            var items = new List<Address>();
            items.Add( new Address() { AddressLine1 = "25A St. Georges Street", Latitude = 52.6327267259, Longitude = 1.2942245012, Postcode = "NR3 1AB", TownCity = "Norwich", County = "Norfolk"} );
            items.Add( new Address() { Organisation = "Art Angels Publishing Ltd", AddressLine1 = "13-15 St. Georges Street", Latitude = 52.6327267259, Longitude = 1.2942245012, Postcode = "NR3 1AB", TownCity = "Norwich", County = "Norfolk"} );
            items.Add( new Address() { AddressLine1 = "2, Tillyard House, 70-72 St. Georges Street", Latitude = 52.6327267259, Longitude = 1.2942245012, Postcode = "NR3 1AB", TownCity = "Norwich", County = "Norfolk"} );
            items.Add( new Address() { AddressLine1 = "Flat 5, The Leather House, 76-78 St. Georges Street", Latitude = 52.6327267259, Longitude = 1.2942245012, Postcode = "NR3 1AB", TownCity = "Norwich", County = "Norfolk"} );            
            return items;
        }
    }
}
