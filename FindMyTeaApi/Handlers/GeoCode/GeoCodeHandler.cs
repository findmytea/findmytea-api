

using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using WebLib.Geo;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryGeoCodeHandler : IQueryHandler<QueryGeoCode, Point>    
    {
        private ILogger<QueryGeoCodeHandler> _logger;
        
        private IGeoService _geoService;

         public QueryGeoCodeHandler(
            ILogger<QueryGeoCodeHandler> logger,
            IGeoService geoService)
        {
            _logger = logger;
            _geoService = geoService;
        }

        public Task<Result<Point>> HandleAsync(QueryGeoCode query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            if (query.SearchTerm is null)
                throw new System.ArgumentNullException("searchTerm");

            return HandleInternalAsync(query);
        }

        private async Task<Result<Point>> HandleInternalAsync(QueryGeoCode query)
        {
            return await Task.Run( () => { 
                Point point =  _geoService.Find(query.SearchTerm);
                return Result.From(point);
            });
        }
    }
}
