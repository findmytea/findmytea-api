
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryBrandByIdHandler : IQueryHandler<QueryBrandById, Brand>
    {
        private readonly ILogger<QueryBrandByIdHandler> _logger;
        private readonly IBrandRepository _repo;

        public QueryBrandByIdHandler(
            ILogger<QueryBrandByIdHandler> logger,
            IBrandRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<Brand>> HandleAsync(QueryBrandById query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<Brand>> HandleInternalAsync(QueryBrandById query)
        {
            return Result.From<Brand>(await _repo.QueryBrandByIdAsync(query.Id));
        }
    }
}