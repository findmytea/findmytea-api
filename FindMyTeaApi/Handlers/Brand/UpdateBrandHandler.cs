﻿using System.Threading.Tasks;
using AutoMapper;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;

namespace FindMyTeaApi.Handlers
{
    public class UpdateBrandHandler : ICommandHandler<UpdateBrand>
    {
        private readonly ILogger<UpdateBrandHandler> _logger;

        private readonly IMapper _mapper;

        private readonly IBrandRepository _repo;

        public UpdateBrandHandler(
            ILogger<UpdateBrandHandler> logger,
            IMapper mapper,
            IBrandRepository repo)
        {
            _logger = logger;
            _mapper = mapper;
            _repo = repo;
        }

        public Task HandleAsync(UpdateBrand command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(UpdateBrand command)
        {
            var brandToUpdate = await _repo.QueryBrandByIdAsync(command.Id);
            _mapper.Map(command, brandToUpdate);
            await _repo.CreateOrUpdateAsync(brandToUpdate);
            command.Id = command.Id;
        }
    }
}
