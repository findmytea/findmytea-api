
using System.Threading.Tasks;
using AutoMapper;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;


namespace FindMyTeaApi.Handlers
{
    public class CreateBrandHandler : ICommandHandler<CreateBrand>
    {
        private readonly ILogger<CreateBrandHandler> _logger;

        private readonly IMapper _mapper;

        private readonly IBrandRepository _repo;

        public CreateBrandHandler(
            ILogger<CreateBrandHandler> logger,
            IMapper mapper,
            IBrandRepository repo)
        {
            _logger = logger;
            _mapper = mapper;
            _repo = repo;
        }

        public Task HandleAsync(CreateBrand command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(CreateBrand command)
        {
            var brand = _mapper.Map<Brand>(command);
            await _repo.CreateOrUpdateAsync(brand);
            command.Id = brand.Id;
        }
    }
}