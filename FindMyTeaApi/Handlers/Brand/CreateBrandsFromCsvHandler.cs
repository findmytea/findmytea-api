using System.IO;
using System.Threading.Tasks;
using FindMyTea.Brand;
using FindMyTea.Image;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using WebLib.Handlers;
using WebLib.S3;

namespace FindMyTeaApi.Handlers
{
    public class CreateBrandsFromCsvHandler : ICommandHandler<CreateBrandsFromCsv>
    {
        private readonly IBrandRepository _repo;

        private readonly IS3Client _s3Client;

        private readonly IImageRepository _imageRepo;

        public CreateBrandsFromCsvHandler(
            IBrandRepository repo,
            IS3Client s3Client,
            IImageRepository imageRepo)
        {
            _repo = repo;
            _s3Client = s3Client;             
            _imageRepo = imageRepo;         
        }

        public Task HandleAsync(CreateBrandsFromCsv command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(CreateBrandsFromCsv command)
        {            
            var imageUploader = new ImageUploader(_s3Client, _imageRepo);
            using(var reader = new StreamReader(command.File.OpenReadStream()))
            {
                await new TwitterBrandImport(_repo, imageUploader).Import(reader, command.User.Id, command.User.TokenId);
            }
        }
    }
}