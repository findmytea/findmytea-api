
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryBrandsHandler : IQueryHandler<QueryBrands, IEnumerable<Brand>>
    {
        private readonly ILogger<QueryBrandsHandler> _logger;
        private readonly IBrandRepository _repo;

        public QueryBrandsHandler(
            ILogger<QueryBrandsHandler> logger,
            IBrandRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<IEnumerable<Brand>>> HandleAsync(QueryBrands query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<IEnumerable<Brand>>> HandleInternalAsync(QueryBrands query)
        {
            return Result.From<IEnumerable<Brand>>(await _repo.QueryAllBrandsAsync());
        }
    }
}