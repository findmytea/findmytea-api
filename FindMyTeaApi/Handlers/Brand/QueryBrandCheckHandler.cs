
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryBrandCheckHandler : IQueryHandler<QueryBrandCheck, bool>
    {
        private readonly ILogger<QueryBrandCheckHandler> _logger;
        private readonly IBrandRepository _repo;

        public QueryBrandCheckHandler(
            ILogger<QueryBrandCheckHandler> logger,
            IBrandRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<bool>> HandleAsync(QueryBrandCheck query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<bool>> HandleInternalAsync(QueryBrandCheck query)
        {
            System.Console.WriteLine(query.Name);
            if (query.TwitterId != null)
            {
                return Result.From<bool>(await _repo.QueryBrandByTwitterIdAsync(query.TwitterId ?? -1) != null);
            }
            else if (!string.IsNullOrEmpty(query.Name))
            {
                return Result.From<bool>(await _repo.QueryBrandByNameIdAsync(query.Name) != null);
            }

            return Result.From<bool>(false);
        }
    }
}