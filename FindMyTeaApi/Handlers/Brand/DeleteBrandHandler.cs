
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;


namespace FindMyTeaApi.Handlers
{
    public class DeleteBrandHandler : ICommandHandler<DeleteBrand>
    {
        private readonly ILogger<DeleteBrandHandler> _logger;

        private readonly IBrandRepository _repo;

        public DeleteBrandHandler(
            ILogger<DeleteBrandHandler> logger,
            IBrandRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task HandleAsync(DeleteBrand command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(DeleteBrand command)
        {
            await _repo.DeleteBrandAsync(command.Id);            
        }
    }
}