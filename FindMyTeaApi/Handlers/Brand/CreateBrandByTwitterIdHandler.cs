using System.Threading.Tasks;
using FindMyTea.Brand;
using FindMyTea.Image;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Configuration;
using WebLib.Handlers;
using WebLib.S3;

namespace FindMyTeaApi.Handlers
{
    public class CreateBrandByTwitterIdHandler : ICommandHandler<CreateBrandByTwitterId>
    {
        private readonly IBrandRepository _repo;

        private readonly IS3Client _s3Client;

        private readonly IImageRepository _imageRepo;

        public CreateBrandByTwitterIdHandler(
            IBrandRepository repo,
            IS3Client s3Client,
            IImageRepository imageRepo)
        {
            _repo = repo;
            _s3Client = s3Client;             
            _imageRepo = imageRepo;         
        }

        public Task HandleAsync(CreateBrandByTwitterId command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(CreateBrandByTwitterId command)
        {
            var imageUploader = new ImageUploader(_s3Client, _imageRepo);
            var brand = await new TwitterBrandImport(_repo, imageUploader).Import(command.TwitterId, command.User.Id, command.User.TokenId);
            command.Id = brand.Id;
        }
    }
}