using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.Models;
using FindMyTeaDal.Entities.Metrics;
using FindMyTeaDal.Repositories;
using Geolocation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using WebLib.Geo;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class SearchesPerDayHandler : IQueryHandler<SearchesPerDay, IEnumerable<SearchPerDay>>
    {
        private readonly IMetricsRepository _repo;
        
        private readonly ILogger<SearchesPerDayHandler> _logger;        
        
        public SearchesPerDayHandler(IMetricsRepository repo, ILogger<SearchesPerDayHandler> logger)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<IEnumerable<SearchPerDay>>> HandleAsync(SearchesPerDay query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<IEnumerable<SearchPerDay>>> HandleInternalAsync(SearchesPerDay query)
        {
            _logger.LogInformation("SearchesPerDay");
            return Result.From<IEnumerable<SearchPerDay>>( await _repo.QuerySearch());
        }
    }
}
