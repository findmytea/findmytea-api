﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.Models;
using FindMyTeaDal.Repositories;
using Geolocation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using WebLib.Geo;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryLocationsSearchHandler : IQueryHandler<QueryLocation, IEnumerable<LocationSearchResult>>
    {
        private readonly IHostEnvironment _env;

        private readonly ILogger<QueryLocationsSearchHandler> _logger;

        private readonly ILocationRepository _repo;

        private readonly IMetricsRepository _metricsRepo;
        private readonly IGeoService _geoService;

        public QueryLocationsSearchHandler(
            IHostEnvironment env,
            ILogger<QueryLocationsSearchHandler> logger,
            ILocationRepository repo,
            IMetricsRepository metricsRepo,
            IGeoService geoService)
        {
            _env = env;
            _logger = logger;
            _repo = repo;
            _metricsRepo = metricsRepo;
            _geoService = geoService;
        }
        public Task<Result<IEnumerable<LocationSearchResult>>> HandleAsync(QueryLocation query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            if (query.SearchTerm == null && (query.Lat == null || query.Lng == null))
            {
                return Task.Run(() => Result.From<IEnumerable<LocationSearchResult>>(new List<LocationSearchResult>()) );
            }                  

            Point point = query.Lat != null && query.Lng != null ? new Point(query.Lat.Value, query.Lng.Value) : _geoService.Find(query.SearchTerm);
            if (point == null)
                return Task.Run( () => Result.From<IEnumerable<LocationSearchResult>>( new List<LocationSearchResult>()) );    

            query.Lat = point.X;
            query.Lng = point.Y;

            return HandleInternalAsync(query, point);
        }

        

        private async Task<Result<IEnumerable<LocationSearchResult>>> HandleInternalAsync(QueryLocation query, Point point)
        {
            var items = await _repo.QueryAllLocationsByDistanceFromAsync(point, query.Distance != null ? query.Distance.Value : 100000);
            await RecordSearchAsync(items.Count());
            return Result.From<IEnumerable<LocationSearchResult>>( items.Select(x => new LocationSearchResult(x, DistanceInMetres(x.GeoLocation, point))));
        }

        private static double DistanceInMetres(Point a, Point b)
        {
            return GeoCalculator.GetDistance(a.X, a.Y, b.X, b.Y, 0, DistanceUnit.Meters);
        }

        private async Task RecordSearchAsync(long numberOfResults)
        {
            if (_env.IsProduction())
            {
                await _metricsRepo.AddSearchAsync(new FindMyTeaDal.Entities.Metrics.Search(numberOfResults));
            }
        }
    }
}
