using System;
using System.Threading.Tasks;
using FindMyTea.Image;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using WebLib.Handlers;
using WebLib.S3;

namespace FindMyTeaApi.Handlers
{
    public class ImageUploadHandler : ICommandHandler<ImageUpload>
    {
        private readonly IS3Client _s3Client;

        private readonly IImageRepository _imageRepo;        

        public ImageUploadHandler(IS3Client s3Client, IImageRepository imageRepo)
        {
            _s3Client = s3Client;             
            _imageRepo = imageRepo;
        }

        public Task HandleAsync(ImageUpload command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            if (command.File is null)
            {
                throw new ArgumentNullException(nameof(command), "File is null");
            }

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(ImageUpload command)
        {
            var originalFileName = CleanupFileName(command.File.FileName);

            var imageUploader = new ImageUploader(_s3Client, _imageRepo);
            var image = await imageUploader.Upload(command.File, originalFileName);            
            command.Image = image;
        }

        private static string CleanupFileName(string fileName)
        {
            return fileName.Replace(" ", "-", StringComparison.OrdinalIgnoreCase).ToLowerInvariant();
        }
    }
}
