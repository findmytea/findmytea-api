using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryImagesHandler : IQueryHandler<QueryImages, IEnumerable<Image>>
    {
        private readonly ILogger<QueryImagesHandler> _logger;

        private readonly IImageRepository _repo;

        public QueryImagesHandler(
            ILogger<QueryImagesHandler> logger,
            IImageRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<IEnumerable<Image>>> HandleAsync(QueryImages query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<IEnumerable<Image>>> HandleInternalAsync(QueryImages query)
        {
            return Result.From( await _repo.QueryImagesAsync());
        }
    }
}
