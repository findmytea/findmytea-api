
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;
using System.Net.Http.Headers;



namespace FindMyTeaApi.Handlers
{
    public class QueryFacebookPagesHandler : IQueryHandler<QueryFacebookPages, FacebookPageResponse>
    {
        private static readonly string _sectionName = "Facebook";

        private static readonly string _facebookGraphUrl = "https://graph.facebook.com";

        private readonly ILogger<QueryFacebookPagesHandler> _logger;

        private readonly string _accessToken;        

        private readonly HttpClient _httpClient;

        public QueryFacebookPagesHandler(IConfiguration config, ILogger<QueryFacebookPagesHandler> logger, HttpClient httpClient)
        {
            var keys = new FacebookKeys();
            config.GetSection(_sectionName).Bind(keys);
            _accessToken = keys.AccessToken;
            _logger = logger;
            _httpClient = httpClient;     
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));        
        }

        public Task<Result<FacebookPageResponse>> HandleAsync(QueryFacebookPages query)
        {            
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

           return QueryFacebookAsync(query);
        }

        private async Task<Result<FacebookPageResponse>> QueryFacebookAsync(QueryFacebookPages query)
        {
            _logger.LogInformation($"QueryFacebookAsync: q={query.SearchTerm}");
            
            var url = $"{_facebookGraphUrl}/search?type=place&q={query.SearchTerm}&limit=1000&fields=id,name,link,about,cover,description,location,phone,email,website&access_token={_accessToken}";
            using var httpResponseMessage = await _httpClient.GetAsync(new Uri(url));

            var results = await httpResponseMessage.Content.ReadAsAsync<FacebookPageResponse>();

            return Result.From((results));
        }
    }
}
