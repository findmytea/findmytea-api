
using System;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Entities.Link;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;


namespace FindMyTeaApi.Handlers
{
    public class UpdateLocationHandler : ICommandHandler<UpdateLocation>
    {
        private readonly ILogger<UpdateLocationHandler> _logger;

        private readonly ILocationRepository _repo;

        public UpdateLocationHandler(
            ILogger<UpdateLocationHandler> logger,
            ILocationRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task HandleAsync(UpdateLocation command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(UpdateLocation command)
        {
            var locationToUpdate = await _repo.QueryLocationByIdAsync(command.Id);

            locationToUpdate.Name = command.Name;
            locationToUpdate.Description = command.Description;
            locationToUpdate.AddressLine1 = command.AddressLine1;
            locationToUpdate.AddressLine2 = command.AddressLine2;
            locationToUpdate.Town = command.Town;
            locationToUpdate.County = command.County;
            locationToUpdate.Postcode = command.Postcode;
            locationToUpdate.Website = command.Website;
            locationToUpdate.Email = command.Email;
            locationToUpdate.Phone = command.Phone;
            locationToUpdate.Image = command.Image;

            locationToUpdate.TwitterId = command.TwitterId ?? 0;
            locationToUpdate.TwitterHandle = command.TwitterHandle;
            locationToUpdate.FacebookId = command.FacebookId ?? 0;
            locationToUpdate.FacebookName = command.FacebookName;
            locationToUpdate.FacebookLink = command.FacebookLink;

            locationToUpdate.Cafe = command.Cafe;
            locationToUpdate.Retailer = command.Retailer;            

            locationToUpdate.UpdateUsername = command.User.Id;
            locationToUpdate.UpdatedUserTokenId = command.User.TokenId;
            locationToUpdate.Updated = DateTime.Now;

            locationToUpdate.GeoLocation = new NetTopologySuite.Geometries.Point(command.GeoX ?? 0, command.GeoY ?? 0);
            locationToUpdate.BrandLocations = command.Brands.Select(b => new  LocationBrand() { BrandId = b.Id } ).ToList();
            locationToUpdate.LocationImages = command.Images.Select(b => new  LocationImage() { ImageId = b.Id } ).ToList();

            await _repo.CreateOrUpdateAsync(locationToUpdate);
            command.Id = command.Id;
        }
    }
}