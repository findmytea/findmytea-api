
using System;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;


namespace FindMyTeaApi.Handlers
{
    public class CreateLocationHandler : ICommandHandler<CreateLocation>
    {
        private readonly ILogger<CreateLocationHandler> _logger;

        private readonly ILocationRepository _repo;

        public CreateLocationHandler(
            ILogger<CreateLocationHandler> logger,
            ILocationRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task HandleAsync(CreateLocation command)
        {
            if (command is null)
                throw new ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(CreateLocation command)
        {
            var now = DateTime.Now;
            var userId = command.User?.Id;
            var userIdToken = command.User?.TokenId;

            var newLocation = new Location( command.Name,
                                            command.Description,
                                            command.AddressLine1,
                                            command.AddressLine2,
                                            command.Town,
                                            command.County,                        
                                            command.Postcode,
                                            Location.UK,
                                            command.Website,
                                            command.Email,
                                            command.Phone,
                                            command.TwitterId ?? 0,
                                            command.TwitterHandle,
                                            command.FacebookId ?? 0,
                                            command.FacebookName,
                                            command.FacebookLink,
                                            command.Image,
                                            command.Retailer,
                                            command.Cafe,
                                            new NetTopologySuite.Geometries.Point(command.GeoX ?? 0, command.GeoY ?? 0),
                                            userId,
                                            userIdToken,
                                            now,
                                            userId,
                                            userIdToken,
                                            now,
                                            command.Brands,
                                            command.Images);

            await _repo.CreateOrUpdateAsync(newLocation);
            command.Id = newLocation.Id;
        }
    }
}