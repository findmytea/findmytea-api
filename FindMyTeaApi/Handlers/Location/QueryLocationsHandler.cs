
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryLocationsHandler : IQueryHandler<QueryLocations, IEnumerable<Location>>
    {
        private readonly ILogger<QueryLocationsHandler> _logger;
        private readonly ILocationRepository _repo;

        public QueryLocationsHandler(
            ILogger<QueryLocationsHandler> logger,
            ILocationRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<IEnumerable<Location>>> HandleAsync(QueryLocations query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<IEnumerable<Location>>> HandleInternalAsync(QueryLocations query)
        {
            return Result.From<IEnumerable<Location>>(await _repo.QueryAllLocationsAsync());
        }
    }
}