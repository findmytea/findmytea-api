
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;


namespace FindMyTeaApi.Handlers
{
    public class DeleteLocationHandler : ICommandHandler<DeleteLocation>
    {
        private readonly ILogger<DeleteLocationHandler> _logger;

        private readonly ILocationRepository _repo;

        public DeleteLocationHandler(
            ILogger<DeleteLocationHandler> logger,
            ILocationRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task HandleAsync(DeleteLocation command)
        {
            if (command is null)
                throw new System.ArgumentNullException(nameof(command));

            return HandleInternalAsync(command);
        }

        private async Task HandleInternalAsync(DeleteLocation command)
        {
            await _repo.DeleteLocationAsync(command.Id);            
        }
    }
}