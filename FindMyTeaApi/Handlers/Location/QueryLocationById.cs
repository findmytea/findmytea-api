
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaDal.Entities;
using FindMyTeaDal.Repositories;
using Microsoft.Extensions.Logging;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryLocationByIdHandler : IQueryHandler<QueryLocationById, Location>
    {
        private readonly ILogger<QueryLocationByIdHandler> _logger;
        private readonly ILocationRepository _repo;

        public QueryLocationByIdHandler(
            ILogger<QueryLocationByIdHandler> logger,
            ILocationRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public Task<Result<Location>> HandleAsync(QueryLocationById query)
        {
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return HandleInternalAsync(query);
        }

        private async Task<Result<Location>> HandleInternalAsync(QueryLocationById query)
        {
            return Result.From<Location>(await _repo.QueryLocationByIdAsync(query.Id));
        }
    }
}