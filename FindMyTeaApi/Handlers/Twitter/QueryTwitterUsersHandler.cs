
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.Models;
using Microsoft.Extensions.Configuration;
using Tweetinvi;
using WebLib.Handlers;
using WebLib.Infrastructure;

namespace FindMyTeaApi.Handlers
{
    public class QueryTwitterUsersHandler : IQueryHandler<QueryTwitterUsers, IEnumerable<TwitterUser>>
    {
        public Task<Result<IEnumerable<TwitterUser>>> HandleAsync(QueryTwitterUsers query)
        {            
            if (query is null)
                throw new System.ArgumentNullException(nameof(query));

            return Task.Run( () => Result.From(Search.SearchUsers(query.SearchTerm).Select(u => new TwitterUser()
                 { Id = u.Id,
                   ScreenName = u.ScreenName,
                   Name = u.Name,
                   Description = u.Description,
                   ImageUrl = u.ProfileImageUrl.Replace("_normal", string.Empty ),
                   SmallImageUrl = u.ProfileImageUrl,
                   Website = u.Url } )));
        }
    }
}
