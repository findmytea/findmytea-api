using System;
using Microsoft.AspNetCore.Mvc;

namespace FindMyTeaApi.Messages.Commands
{
    public class UpdateLocation : CreateLocation
    {
        [FromQuery]
        public new Guid Id { get; set; }
    }
}
