using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using WebLib.Messages;
using WebLib.Authentication;
using FindMyTeaDal.Entities;
using System.Collections.Generic;

namespace FindMyTeaApi.Messages.Commands
{
    public class CreateBrand : ICommand, IAuthorizedMessage
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public long TwitterId { get; set; }

        public string TwitterHandle { get; set; }

        public long? FacebookId { get; set; }

        public string FacebookName { get; set; }

        public string FacebookLink { get; set; }        

        public Image Image { get; set; }

        public IEnumerable<Image> Images { get; set; }

        [JsonIgnore]
        public Guid Id { get; set; }

        public AuthUserInfo User { get; set; }
    }
}
