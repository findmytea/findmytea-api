using System;
using FindMyTeaDal.Entities;
using Microsoft.AspNetCore.Http;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Commands
{
    public class ImageUpload : ICommand
    {
        public IFormFile File { get; set; }

        public Image Image { get; set; }
    }
}