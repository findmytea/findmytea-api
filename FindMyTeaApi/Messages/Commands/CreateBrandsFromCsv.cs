
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebLib.Authentication;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Commands
{
    public class CreateBrandsFromCsv : ICommand, IAuthorizedMessage
    {
        [Required]
        public IFormFile File { get; set; }

        public AuthUserInfo User { get; set; }
    }
}