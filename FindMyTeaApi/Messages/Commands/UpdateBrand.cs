﻿
using System;
using Microsoft.AspNetCore.Mvc;

namespace FindMyTeaApi.Messages.Commands
{
    public class UpdateBrand : CreateBrand
    {
        [FromQuery]
        public new Guid Id { get; set; }
    }
}
