
using System;
using Microsoft.AspNetCore.Mvc;
using WebLib.Authentication;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Commands
{
    public class DeleteLocation : ICommand, IAuthorizedMessage
    {
         [FromRoute]
         public Guid Id { get; set; }

        public AuthUserInfo User { get; set; }
    }
}