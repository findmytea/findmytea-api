
using System;
using Microsoft.AspNetCore.Mvc;
using WebLib.Authentication;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Commands
{
    public class DeleteBrand : ICommand, IAuthorizedMessage
    {
         [FromRoute]
         public Guid Id { get; set; }

        public AuthUserInfo User { get; set; }
    }
}
