using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using WebLib.Messages;
using WebLib.Authentication;
using System.Collections.Generic;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Messages.Commands
{
    public class CreateLocation : ICommand, IAuthorizedMessage
    {
        public string Name { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool Retailer { get; set; }
        
        public bool Cafe { get; set; }

        public string Description { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public string Country { get; set; }

        public Image Image { get; set; }

        public double? GeoX { get; set; }

        public double? GeoY { get; set; }

        public long? TwitterId { get; set; }

        public string TwitterHandle { get; set; }

        public long? FacebookId { get; set; }

        public string FacebookName { get; set; }

        public string FacebookLink { get; set; }

        public IEnumerable<Brand> Brands {get; set; }

        public IEnumerable<Image> Images { get; set; }

        [JsonIgnore]
        public Guid Id { get; set; }

        public AuthUserInfo User { get; set; }
    }
}
