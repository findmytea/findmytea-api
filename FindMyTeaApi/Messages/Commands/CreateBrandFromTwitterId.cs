using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using WebLib.Authentication;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Commands
{
    public class CreateBrandByTwitterId : ICommand, IAuthorizedMessage
    {
        [Required]
        public long TwitterId { get; set; }

        [JsonIgnore]
        public Guid Id { get; set; }

        public AuthUserInfo User { get; set; }
    }
}