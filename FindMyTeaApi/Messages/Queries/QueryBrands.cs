using System.Collections.Generic;
using FindMyTeaDal.Entities;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryBrands : IQuery<IEnumerable<Brand>>
    {}
}