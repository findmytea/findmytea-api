using System.Collections.Generic;
using FindMyTeaDal.Entities.Metrics;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class SearchesPerDay : IQuery<IEnumerable<SearchPerDay>>
    {}
}