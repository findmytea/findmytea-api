
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FindMyTeaApi.Models;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryLocation : IQuery<IEnumerable<LocationSearchResult>>
    {
        public string SearchTerm { get; set; }

        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public long? Distance { get; set; }
    }
}   