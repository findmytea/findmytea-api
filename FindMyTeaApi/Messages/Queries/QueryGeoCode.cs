
using NetTopologySuite.Geometries;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryGeoCode : IQuery<Point>
    {
        public string SearchTerm { get; set; }
    }
}