using System;
using FindMyTeaDal.Entities;
using Microsoft.AspNetCore.Mvc;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryBrandById : IQuery<Brand>
    {
         [FromRoute]
         public Guid Id { get; set; }
    }
}