using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryBrandCheck : IQuery<bool>
    {
        public long? TwitterId { get; set; }

        public string Name { get; set; }
    }
}