using System.Collections.Generic;
using FindMyTeaDal.Entities;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryLocations : IQuery<IEnumerable<Location>>
    {}
}