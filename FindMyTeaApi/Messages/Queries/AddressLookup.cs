using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FindMyTeaApi.Models;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class AddressLookup : IQuery<IEnumerable<Address>>
    {
        [Required]
        public string SearchTerm { get; set; }
    }
}