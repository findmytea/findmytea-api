
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FindMyTeaApi.Models;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryTwitterUsers : IQuery<IEnumerable<TwitterUser>>
    {
        [Required]
        public string SearchTerm { get; set; }
    }
}   