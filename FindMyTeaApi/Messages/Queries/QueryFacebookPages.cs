
using System.ComponentModel.DataAnnotations;
using FindMyTeaApi.Models;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryFacebookPages : IQuery<FacebookPageResponse>
    {
        [Required]
        public string SearchTerm { get; set; }
    }
}   