using System;
using FindMyTeaDal.Entities;
using Microsoft.AspNetCore.Mvc;
using WebLib.Messages;

namespace FindMyTeaApi.Messages.Queries
{
    public class QueryLocationById : IQuery<Location>
    {
         [FromRoute]
         public Guid Id { get; set; }
    }
}