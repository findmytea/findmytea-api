using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Lib.Authentication;
using FindMyTeaApi.Lib.Authentication.ApiKey;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebLib.Controllers;
using WebLib.Infrastructure;
using WebLib.MessageBus;

namespace Snoop.References.Web.Controllers
{
    [Route("AddressLookup")]
    [ApiController]
    [Authorize(AuthenticationPolicyExtensions.AddressLookup, AuthenticationSchemes = ApiKeyAuthenticationOptions.DefaultScheme + "," + JwtBearerDefaults.AuthenticationScheme)]
    public class AddressLookupController : BaseController
    {
        public AddressLookupController(IDispatcher dispatcher)
            : base(dispatcher)
        { }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<AddressViewModel>))]
        public Task<IActionResult> AddressLookupByPostcodeAsync([FromQuery] AddressLookup query)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return AddressLookupByPostcodeInternalAsync(query);
        }

        private async Task<IActionResult> AddressLookupByPostcodeInternalAsync(AddressLookup query)
        {
            var items = await QueryAsync(query);
            return items.Succeeded ? new Result<IEnumerable<AddressViewModel>>(items.Value.Select(a => new AddressViewModel(a))).ToActionResult() : items.ToActionResult();
        }
    }
}
