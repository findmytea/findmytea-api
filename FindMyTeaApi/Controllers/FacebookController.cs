using System.Linq;
using System.Threading.Tasks;
using FindMyTeaApi.Lib.Authentication;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebLib.Controllers;
using WebLib.Infrastructure;
using WebLib.MessageBus;

namespace FindMyTeaApi.Controllers
{
    [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
    [ApiController]
    [Route("[controller]")]
    public class FacebookController : BaseController
    {
        public FacebookController(IDispatcher dispatcher)
            : base(dispatcher)
        { }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(FacebookPageSearchResponse))]
        public async Task<IActionResult> SearchUsersAsync([FromQuery] QueryFacebookPages query)
        {
            var items = await QueryAsync(query);
            return Result.From(new FacebookPageSearchResponse( items.Value.Data.Select(p => new FacebookPageViewModel(p)))).ToActionResult();
        }
    }
}