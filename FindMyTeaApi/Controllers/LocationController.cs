

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using FindMyTeaApi.Messages.Commands;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using System;
using FindMyTeaApi.Lib.Authentication;

namespace FindMyTeaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationController : BaseController
    {
        private readonly ILogger<LocationController> _logger;

        private readonly IMapper _mapper;

        private readonly string _imagePath;

        public LocationController(IDispatcher dispatcher, ILogger<LocationController> logger, IMapper mapper, IConfiguration config)
            : base(dispatcher)
        {
            _logger = logger;
            _mapper = mapper;
            _imagePath = config["IMAGE_CDN_URL"];
        }

        [HttpGet]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(LocationResponse<LocationViewModel>))]
        public Task<IActionResult> LocationsAsync([FromQuery] QueryLocations query)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return LocationsInternalAsync(query);
        }

        private async Task<IActionResult> LocationsInternalAsync(QueryLocations query)
        {
            var items = await QueryAsync(query);
            return Result.From(new LocationResponse<LocationViewModel>(items.Value.Select(l => _mapper.Map<LocationViewModel>(l, opt => MappingOptions(opt))))).ToActionResult();
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteLocationAsync([FromQuery] DeleteLocation command)
        {
            _logger.LogInformation($"DeleteBrandAsync: Id={command.Id}, user=({command.User})");

            await SendAsync(command);
            return Ok();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public Task<IActionResult> GetLocationAsync([FromQuery] QueryLocationById query)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return GetLocationInternalAsync(query);
        }

        private async Task<IActionResult> GetLocationInternalAsync(QueryLocationById query)
        {
            _logger.LogInformation($"GetLocationAsync: Id={query.Id})");

            var item = await QueryAsync(query);
            return Result.From(_mapper.Map<LocationDetailViewModel>(item.Value, opt => MappingOptions(opt))).ToActionResult();
        }

        [HttpPost()]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> CreateLocationAsync([FromBody] CreateLocation command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return CreateLocationInternalAsync(command);
        }

        private async Task<IActionResult> CreateLocationInternalAsync([FromBody] CreateLocation command)
        {
            _logger.LogInformation($"CreateLocationAsync: name={command.Name}, user=({command.User})");

            await SendAsync(command);
            return await GetLocationInternalAsync(new QueryLocationById() { Id = command.Id });            
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> UpdateLocationAsync([FromBody] UpdateLocation command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return UpdateLocationInternalAsync(command);
        }

        private async Task<IActionResult> UpdateLocationInternalAsync(UpdateLocation command)
        {
            _logger.LogInformation($"UpdateLocationAsync: id={command.Id}, name={command.Name}, user=({command.User})");

            await SendAsync(command);
            return await GetLocationInternalAsync(new QueryLocationById() { Id = command.Id });
        }

        private void MappingOptions(IMappingOperationOptions opt)
            => opt.Items["imagePath"] = _imagePath;
    }
}
