using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using FindMyTeaApi.Messages.Commands;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System;
using AutoMapper;
using FindMyTeaApi.Lib.Authentication;

namespace FindMyTeaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrandController : BaseController
    {
        private ILogger<BrandController> _logger;

        private readonly IMapper _mapper;

        private readonly string _imagePath;

        public BrandController(IDispatcher dispatcher, ILogger<BrandController> logger, IMapper mapper, IConfiguration config)
            : base(dispatcher)
        {
            _logger = logger;
            _mapper = mapper;
            _imagePath = config["IMAGE_CDN_URL"];
         }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(BrandResponse))]
        public async Task<IActionResult> BrandsAsync([FromQuery] QueryBrands query)
        {
            var items = await QueryAsync(query);
            return Result.From(new BrandResponse( items.Value.Select(b => _mapper.Map<BrandSummaryViewModel>(b, opt => MappingOptions(opt))))).ToActionResult();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public Task<IActionResult> GetBrandAsync([FromQuery] QueryBrandById query)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return GetBrandInternalAsync(query);
        }

        private async Task<IActionResult> GetBrandInternalAsync(QueryBrandById query)
        {
            _logger.LogInformation($"GetBrandAsync: Id={query.Id})");

            var item = await QueryAsync(query);
            return Result.From(_mapper.Map<BrandDetailViewModel>(item.Value, opt => MappingOptions(opt))).ToActionResult();
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteBrandAsync([FromQuery] DeleteBrand command)
        {
            _logger.LogInformation($"DeleteBrandAsync: Id={command.Id}, user=({command.User})");

            await SendAsync(command);
            return Ok();
        }

        [HttpPost()]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> CreateBrandAsync([FromBody] CreateBrand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return CreateBrandInternalAsync(command);
        }

        private async Task<IActionResult> CreateBrandInternalAsync(CreateBrand command)
        {
            _logger.LogInformation($"CreateBrandAsync: name={command.Name}, user=({command.User})");

            await SendAsync(command);
            return await GetBrandInternalAsync(new QueryBrandById() { Id = command.Id });
        }

        [HttpPost()]
        [Route("Twitter")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> CreateBrandAsync([FromBody] CreateBrandByTwitterId command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return CreateBrandInternalAsync(command);
        }

        private async Task<IActionResult> CreateBrandInternalAsync(CreateBrandByTwitterId command)
        {
            _logger.LogInformation($"CreateBrandAsync: twitterId={command.TwitterId}, user=({command.User})");

            await SendAsync(command);
            return await GetBrandInternalAsync(new QueryBrandById() { Id = command.Id });
        }

        [HttpPost()]
        [Route("Csv")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> CreateBrandsFromCsvAsync([FromForm] CreateBrandsFromCsv command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return CreateBrandsFromCsvInternalAsync(command);
        }

        private async Task<IActionResult> CreateBrandsFromCsvInternalAsync(CreateBrandsFromCsv command)
        {
            _logger.LogInformation($"CreateBrandsFromCsvAsync: filename={command.File.Name}, user=({command.User})");

            await SendAsync(command);
            return Ok();
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public Task<IActionResult> UpdateBrandAsync([FromBody] UpdateBrand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return UpdateBrandInternalAsync(command);
        }

        private async Task<IActionResult> UpdateBrandInternalAsync(UpdateBrand command)
        {
            _logger.LogInformation($"UpdateBrandAsync: name={command.Name}, user=({command.User})");

            await SendAsync(command);
            return await GetBrandInternalAsync(new QueryBrandById() { Id = command.Id });
        }

        [HttpGet("Check")]
        [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolResponse))]
        public async Task<IActionResult> CheckAsync([FromQuery] QueryBrandCheck query)
        {
            var result = await QueryAsync(query);
            return Result.From(new BoolResponse( result.Value )).ToActionResult();
        }

        private void MappingOptions(IMappingOperationOptions opt)
            => opt.Items["imagePath"] = _imagePath;
    }
}