

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using FindMyTeaApi.Messages.Commands;
using FindMyTeaApi.Lib.Authentication;

namespace FindMyTeaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
    public class ImageController : BaseController
    {
        private readonly ILogger<ImageController> _logger;

        private readonly string _imagePath;

        public ImageController(IDispatcher dispatcher, ILogger<ImageController> logger, IConfiguration config)
            : base(dispatcher)
        {
            _logger = logger;
            _imagePath = config["IMAGE_CDN_URL"];
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(IEnumerable<ImageViewModel>))]
         public Task<IActionResult> SearcImagesAsync([FromQuery] QueryImages query)
         {
            if (query is null)
                throw new ArgumentNullException(nameof(query));

            _logger.LogInformation($"SearcImagesAsync");
            return SearchLcationsInternalAsync(query);
         }

         private async Task<IActionResult> SearchLcationsInternalAsync([FromQuery] QueryImages query)
         {
            var items = await QueryAsync(query);
            return Result.From(items.Value.Select(i => new ImageViewModel(i, _imagePath)) ).ToActionResult();
         }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(UploadViewModel))]
        public Task<IActionResult> UploadAsync([FromForm] ImageUpload command)
        {
            if (command is null)
            {
                throw new System.ArgumentNullException(nameof(command));
            }

            return UploadInternalAsync(command);
        }

        private async Task<IActionResult> UploadInternalAsync(ImageUpload command)
        {
            await SendAsync(command);
            return Result.From(new ImageViewModel(command.Image, _imagePath) ).ToActionResult();
        }
    }
}
