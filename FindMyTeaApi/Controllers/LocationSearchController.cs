

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.Extensions.Configuration;
using AutoMapper;

namespace FindMyTeaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationSearchController : BaseController
    {
        private readonly ILogger<LocationSearchController> _logger;

        private readonly IMapper _mapper;

        private readonly string _imagePath;

        public LocationSearchController(IDispatcher dispatcher, ILogger<LocationSearchController> logger, IMapper mapper, IConfiguration config)
            : base(dispatcher)
        {
            _logger = logger;
            _imagePath = config["IMAGE_CDN_URL"];
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(LocationResponse<LocationSearchResultViewModel>))]
         public Task<IActionResult> SearchLcationsAsync([FromQuery] QueryLocation query)
         {
            if (query is null)
                throw new ArgumentNullException(nameof(query));

            _logger.LogInformation($"SearchLcationsAsync: searchTerm={query.SearchTerm} lat={query.Lat} lng={query.Lng}");
            return SearchLcationsInternalAsync(query);
         }

         private async Task<IActionResult> SearchLcationsInternalAsync([FromQuery] QueryLocation query)
         {
            var items = await QueryAsync(query);
            var locationResults = items.Value.Select(l => _mapper.Map<LocationSearchResultViewModel>(l, opt => MappingOptions(opt))).OrderBy(l => l.Distance);
            var result = new LocationResponse<LocationSearchResultViewModel>(locationResults, query.Lat, query.Lng);
            return Result.From(result).ToActionResult();
         }

         private void MappingOptions(IMappingOperationOptions opt)
            => opt.Items["imagePath"] = _imagePath;
    }
}
