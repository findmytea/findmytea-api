
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FindMyTeaApi.Lib.Authentication;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebLib.Controllers;
using WebLib.Infrastructure;
using WebLib.MessageBus;

namespace Snoop.References.Web.Controllers
{
    [Route("GeoCode")]
    [ApiController]
    [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
    public class GeoCodeController : BaseController
    {
        public GeoCodeController(IDispatcher dispatcher)
            : base(dispatcher)
        { }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GeoViewModel>))]
        public Task<IActionResult> GeoCodeLookupAsync([FromQuery] QueryGeoCode query)
        {
            if (query is null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            return GeoCodeLookupInternalAsync(query);
        }

        private async Task<IActionResult> GeoCodeLookupInternalAsync(QueryGeoCode query)
        {
            var item = await QueryAsync(query);
            return new Result<GeoViewModel>(new GeoViewModel() { GeoX = item.Value.X, GeoY = item.Value.Y } ).ToActionResult();
        }
    }
}
