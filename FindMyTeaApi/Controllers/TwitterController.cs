

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using FindMyTeaApi.Lib.Authentication;

namespace FindMyTeaApi.Controllers
{
    [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
    [ApiController]
    [Route("[controller]")]
    public class TwitterController : BaseController
    {
        public TwitterController(IDispatcher dispatcher)
            : base(dispatcher)
        { }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(TwitterUserSerachResponse))]
        public async Task<IActionResult> SearchUsersAsync([FromQuery] QueryTwitterUsers query)
        {
            var items = await QueryAsync(query);
            return Result.From(new TwitterUserSerachResponse( items.Value.Select(t => new TwitterUserViewModel(t)))).ToActionResult();
        }
    }
}