

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FindMyTeaApi.Messages.Queries;
using FindMyTeaApi.ViewModels;
using WebLib.Controllers;
using WebLib.MessageBus;
using System.Threading.Tasks;
using WebLib.Infrastructure;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using FindMyTeaApi.Messages.Commands;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System;
using AutoMapper;
using FindMyTeaApi.Lib.Authentication;

namespace FindMyTeaApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationPolicyExtensions.IsAdmin)]
    public class MetricsController : BaseController
    {
        private ILogger<MetricsController> _logger;

        private readonly IMapper _mapper;

        public MetricsController(IDispatcher dispatcher, ILogger<MetricsController> logger, IMapper mapper)
            : base(dispatcher)
        {
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("SearchesPerDay")]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(SearchesPerDayResponse))]
        public async Task<IActionResult> SearchesPerDayAsync([FromQuery] SearchesPerDay query)
        {
            var items = await QueryAsync(query);
            return Result.From(new SearchesPerDayResponse( items.Value.Select(_mapper.Map<SearchPerDayViewModel>))).ToActionResult();
        }
    }
}
