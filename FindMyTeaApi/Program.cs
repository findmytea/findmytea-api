using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore;
using Serilog;
using Serilog.Events;
using System;
using SumoLogic.Logging.Serilog.Extensions;
using FindMyTeaApi.Logging;
using System.Collections;
using Serilog.Formatting.Compact;

namespace FindMyTeaApi
{
    public class Program
    {
        public static int Main(string[] args)
        {
             Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Verbose)
                .Enrich.FromLogContext()               
                .WriteTo.Console()
                .CreateLogger();    
            
            try
            {
                foreach (DictionaryEntry de in Environment.GetEnvironmentVariables())
                {
                    Log.Logger.Information(de.Key + ": " + de.Value);
                }
                
                BuildWebHost(args).Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Logger.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHost BuildWebHost(string[] args)
        {
                var builder = WebHost.CreateDefaultBuilder(args)
                        .ConfigureAppConfiguration((hostingContext, config) =>  
                        {
                        })
                        .UseStartup<Startup>()
                        .UseUrls("http://*:5000", "https://*:5001");

                if (!IsDevelopment() && !IsTest())
                {
                    builder.UseLogging();
                }

                return builder.Build();
        }


        private static bool IsDevelopment()
            => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

        private static bool IsTest()
            => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Test";
    }

    
}
