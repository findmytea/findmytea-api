
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FindMyTea.Image
{
    public interface IImageUploader
    {
        Task<FindMyTeaDal.Entities.Image> Upload(Uri imageUri);

        Task<FindMyTeaDal.Entities.Image> Upload(IFormFile file, string originalFileName);
    }
}