
using System;
using System.Threading.Tasks;
using FindMyTeaDal.Repositories;
using Microsoft.AspNetCore.Http;
using WebLib.S3;

namespace FindMyTea.Image
{
    public class ImageUploader : IImageUploader
    {
        private const string BucketName = "findmytea";

        private const string Path = "images";

        private readonly IS3Client _s3Client;

        private readonly IImageRepository _imageRepo;

        public ImageUploader(IS3Client s3Client, IImageRepository imageRepo)
        {
            _s3Client = s3Client;
            _imageRepo = imageRepo;
        }

        public async Task<FindMyTeaDal.Entities.Image> Upload(Uri imageUri)
        {
            var image = await CreateImage(System.IO.Path.GetFileName(imageUri.LocalPath));
            await _imageRepo.CreateOrUpdateAsync(image);
            
            await _s3Client.UploadAsync(BucketName, image.LocalPath, imageUri);
            return image;
        }

        public async Task<FindMyTeaDal.Entities.Image> Upload(IFormFile file, string originalFileName)
        {
            var image = await CreateImage(originalFileName);            
            await _s3Client.UploadAsync(BucketName, image.LocalPath, file);            
            return image;
        }

        private async Task<FindMyTeaDal.Entities.Image> CreateImage(string originalFileName)
        {
            var image = new FindMyTeaDal.Entities.Image() { OriginalFileName = originalFileName, Path = Path };            
            await _imageRepo.CreateOrUpdateAsync(image);
            return image;
        } 
            
    }
}
