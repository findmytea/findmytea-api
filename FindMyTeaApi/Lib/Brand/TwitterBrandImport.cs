
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using CsvHelper;
using FindMyTea.Image;
using FindMyTeaDal.Entities.Link;
using FindMyTeaDal.Repositories;
using Tweetinvi;
using Tweetinvi.Models;

namespace FindMyTea.Brand
{
    public class TwitterBrandImport
    {
        private readonly IBrandRepository _brandRepo;

        private readonly IImageUploader _imageUploader;

        public TwitterBrandImport(
            IBrandRepository brandRepo,
            IImageUploader imageUploader)
        {
            _brandRepo = brandRepo;
            _imageUploader = imageUploader;
        }

        private async Task<FindMyTeaDal.Entities.Brand> GetBrand(IUser user, string username, string userId)
        {
            var brand = await _brandRepo.QueryBrandByTwitterIdAsync(user.Id);
            if (brand is null)
            {
                brand = new FindMyTeaDal.Entities.Brand() {
                        TwitterId = user.Id,
                        TwitterHandle = user.ScreenName,
                        Website = user.Url,
                        BrandImages = new List<BrandImage>(),
                        Username = username,
                        Created = DateTime.Now,
                        UserTokenId = userId
                 };
            }
            return brand;
        }

        public async Task<FindMyTeaDal.Entities.Brand> Import(long twitterId, string username, string userId)
        {
            var user = User.GetUserFromId(twitterId);
            var imageUri = new Uri(user.ProfileImageUrlFullSize);
            var image = await _imageUploader.Upload(imageUri);

            var brand = await GetBrand(user, username, userId);
            brand.Name = user.Name;
            brand.Description = user.Description;
            brand.TwitterHandle = user.ScreenName;
            brand.Website = await resolveUrl(user.Url);

            brand.UpdateUsername = username;
            brand.UpdatedUserTokenId = userId;
            brand.Updated = DateTime.Now;

            brand.BrandImages.Add(new BrandImage() { Image = image });
            brand.Image = image; 

            await _brandRepo.CreateOrUpdateAsync(brand);
            return brand;
        }

        private async Task<string> resolveUrl(string url)
        {
            try
            {            
                // TODO: Inject HttpClient
                using var client = new HttpClient();
                using var response = await client.GetAsync(url);
                return response.RequestMessage.RequestUri.ToString();
            }
            catch(Exception)
            {
                // Log
                return url;
            }
        }

        private class CsvRecord
        {
            public string TwitterHandle {get; set;}
        } 

        public async Task<ICollection<FindMyTeaDal.Entities.Brand>> Import(StreamReader csvStream, string username, string userId)
        {
            var brands = new List<FindMyTeaDal.Entities.Brand>();
            using (var csv = new CsvReader(csvStream, CultureInfo.InvariantCulture))
            {
               foreach(var line in csv.GetRecords<CsvRecord>())
               {
                    var user = User.GetUserFromScreenName(line.TwitterHandle);
                    if (user != null)
                    { 
                        brands.Add(await Import(user.Id, username, userId));
                    }
                  
               }
            }
            return brands;
        }
    }
}