﻿using Microsoft.Extensions.DependencyInjection;

namespace FindMyTeaApi.Lib.Authentication
{
    public static class AuthenticationPolicyExtensions
    {
        public const string RoleClaimType = "fmi:roles";

        public const string IsAdmin = "admin";

        private const string AdminRole = "admin";

        public const string AddressLookup = "addressLookup";

        private const string AddressLookupRole = "addressLookup";

        public static void ConfigureAuthPolicies(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(IsAdmin, policyBuilder => policyBuilder.RequireClaim(RoleClaimType, AdminRole));
                options.AddPolicy(AddressLookup, policyBuilder => policyBuilder.RequireClaim(RoleClaimType, AdminRole, AddressLookupRole));
            });
        }        
    }
}
