﻿using System;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using SumoLogic.Logging.Serilog.Extensions;

namespace FindMyTeaApi.Logging
{
    public static class LoggingExtensions
    {
        public static IWebHostBuilder UseLogging(this IWebHostBuilder webHostBuilder)
            => webHostBuilder.UseSerilog((context, loggerConfiguration) =>
            {
                 loggerConfiguration.WriteTo.SumoLogic(
                    new Uri(context.Configuration["SUMO_URL"]),
                    sourceName: "FindMyTeaApi");
            });
    }
}
