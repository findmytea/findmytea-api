using Newtonsoft.Json;

namespace FindMyTea.Postcoder
{
    /// <summary>
    /// For storing the results of an address lookup
    /// </summary>
    [JsonObject]
    public class AddressLookup
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SummaryLine { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string POBox { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Organisation { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DepartmentName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string BuilidingName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SubbuildingName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Premise { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DependentStreet { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Street { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DoubleDependentLocality { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DependentLocality { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PostTown { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string County { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Postcode { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Recodes { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? MoreValues { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? NextPage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalResults { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Alias { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double Longitude { get; set;}

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double Latitude { get; set;}
    }
}