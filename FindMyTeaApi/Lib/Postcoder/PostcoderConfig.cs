namespace FindMyTea.Postcoder
{
    public class PostcoderConfig
    {
        public string ApiKey { get; set; }

        public string CountryCode { get; set; }
    }
}