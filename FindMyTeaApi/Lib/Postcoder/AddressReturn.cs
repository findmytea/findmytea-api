using System.Collections.Generic;
using Newtonsoft.Json;

namespace FindMyTea.Postcoder
{
    /// <summary>
    /// A wrapper class for returning the results of an address lookup
    /// with some summary information included
    /// </summary>
    [JsonObject]
    public class AddressReturn
    {
        [JsonProperty]
        public int AddressCount { get; set; }

        [JsonProperty]
        public int CurrentPage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? NextPage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; private set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<AddressLookup> Addresses { get; private set; }

        public static AddressReturn WithError(string error)
        {
            return new AddressReturn() { Error = error };
        }

        public static AddressReturn WithAddresses(List<AddressLookup> addresses)
        {
            return new AddressReturn() { Addresses = addresses };
        }
    }
}