using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace FindMyTea.Postcoder
{
    public class Postcoder : IPostcoder
    {
        private static string _sectionName = "Postcoder";

        private readonly HttpClient _client;

        private readonly string _countryCode;

        private readonly string _apiKey;

        private readonly int _page = 0;

        public Postcoder(IConfiguration config, HttpClient client)
        {
            if (config is null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            _client = client;

            var pcConfig = new PostcoderConfig();
            config.GetSection(_sectionName).Bind(pcConfig);

            _apiKey = pcConfig.ApiKey;
            _countryCode = pcConfig.CountryCode;
        }

        public Task<AddressReturn> AddressLookup(string postcode)
        {
            if (postcode is null)
            {
                throw new ArgumentNullException(nameof(postcode));
            }

            postcode = postcode.Trim();
            postcode = HttpUtility.UrlEncode(postcode);

            if (string.IsNullOrEmpty(postcode))
            {
                throw new ArgumentNullException(nameof(postcode));
            }

            return AddressLookupInternal(postcode);
        }

        private async Task<AddressReturn> AddressLookupInternal(string postcode)
        {
            string addressUrl = $"https://ws.postcoder.com/pcw/{_apiKey}/address/{_countryCode}/{postcode}?page={_page}&addtags=longitude&addtags=latitude";
            _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            using HttpResponseMessage resp = await _client.GetAsync(new Uri(addressUrl));

            if (!resp.IsSuccessStatusCode)
            {
                return AddressReturn.WithError($"An error occurred - {resp.ReasonPhrase}");
            }

            var addressList = JsonConvert.DeserializeObject<List<AddressLookup>>(await resp.Content.ReadAsStringAsync());

            if (addressList.Count == 0)
            {
                return AddressReturn.WithAddresses(new List<AddressLookup>());
            }

            var output = AddressReturn.WithAddresses(addressList);
            output.CurrentPage = _page;

            if (output.Addresses.Last().MoreValues ?? false)
            {
                output.AddressCount = output.Addresses.Last().TotalResults.Value;
                output.NextPage = output.Addresses.Last().NextPage;
            }
            else
            {
                output.AddressCount = addressList.Count;
            }

            return output;
        }
    }
}