using System.Threading.Tasks;

namespace FindMyTea.Postcoder
{
    public interface IPostcoder
    {
        public Task<AddressReturn> AddressLookup(string postcode);
    }
}