namespace FindMyTeaApi.Models
{
    public class FacebookPage
    {
        public long Id {get; set; }

        public string Name { get; set; }
       
        public string Link { get; set; }

        public string About { get; set; }
    }
}