
using System;
using FindMyTea.Postcoder;

namespace FindMyTeaApi.Models
{
    public class Address
    {
        public Address()
        { }

        public Address(AddressLookup address)
        {
            if (address is null)
            {
                throw new ArgumentNullException(nameof(address));
            }

            Organisation = NullIfEmpty(address.Organisation);
            Premise = NullIfEmpty(Clean(address.Premise));
            Street = NullIfEmpty(Clean(address.Street));
            AddressLine1 = NullIfEmpty($"{Premise} {Street}");
            DependentLocality = NullIfEmpty(Clean(address.DependentLocality));
            DoubleDependentLocality = NullIfEmpty(Clean(address.DoubleDependentLocality));
            AddressLine2 = NullIfEmpty($"{DependentLocality} {DoubleDependentLocality}");
            TownCity = NullIfEmpty(address.PostTown);
            County = NullIfEmpty(address.County);
            Postcode = NullIfEmpty(address.Postcode);
            Longitude = address.Longitude;
            Latitude = address.Latitude;
        }

        public string Organisation { get; set; }

        public string Premise { get; set; }

        public string Street { get; set; }

        public string AddressLine1 { get; set; }

        public string DependentLocality { get; set; }

        public string DoubleDependentLocality { get; set; }

        public string AddressLine2 { get; set; }

        public string TownCity { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public double Longitude { get; set;}

        public double Latitude { get; set;}

        private static string Clean(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            return $"{s.Trim()} ";
        }

        private static string NullIfEmpty(string s)
        {
            return string.IsNullOrWhiteSpace(s) ? null : s.Trim();
        }
    }
}