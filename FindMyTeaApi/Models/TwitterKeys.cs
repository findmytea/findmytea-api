
namespace FindMyTeaApi.Models
{
    public class TwitterKeys
    {
        public const string SectionName = "Twitter";

        public string APIkey {get; set;}

        public string APISecretKey {get; set;}

        public string AccessToken {get; set;}

        public string AccessTokenSecret {get; set;}
    }
}