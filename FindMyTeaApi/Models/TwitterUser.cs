namespace FindMyTeaApi.Models
{
    public class TwitterUser
    {
        public long Id {get; set; }

        public string ScreenName { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string SmallImageUrl { get; set; }

        public string Website { get; set; }
    }
}