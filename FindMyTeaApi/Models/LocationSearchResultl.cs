using System;
using System.Collections.Generic;
using System.Linq;
using FindMyTeaDal.Entities;

namespace FindMyTeaApi.Models
{
    public class LocationSearchResult : IHasImage
    {
        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Website { get; private set; }

        public bool Retailer { get; private set; }
        
        public bool Cafe { get; private set; }

        public double? Distance { get; private set; }

        public Image Image { get; set; }

        public string Description { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public string Country { get; set; }

        public double? GeoX {get; set;}

        public double? GeoY {get; set;}

        public IEnumerable<Brand> Brands {get; set; }       

        public LocationSearchResult(Location location, double? distance)
        {
            if (location is null)
                throw new ArgumentNullException(nameof(location));

            Id = location.Id;
            Name = location.Name;
            Description = location.Description;
            AddressLine1 = location.AddressLine1;
            AddressLine2 = location.AddressLine2;
            Town = location.Town;
            County = location.County;
            Postcode = location.Postcode;
            Website = location.Website;
            Retailer = location.Retailer;
            Cafe = location.Cafe;
            GeoX = location.GeoLocation.X;
            GeoY = location.GeoLocation.Y;
            Distance = distance;
            Image = location.Image;

            Brands = location.BrandLocations.Select(b => b.Brand);
        }
    }
}
