
using System.Collections.Generic;

namespace FindMyTeaApi.Models
{
    public class FacebookPageResponse
    {
        public IEnumerable<FacebookPage> Data {get; set; }
    }
}

