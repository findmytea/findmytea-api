const { Pool } = require("pg");
var request = require("request").defaults({ encoding: null });
const AWS = require("aws-sdk");
const { v4 } = require("uuid");

const pool = () =>
  new Pool({
    user: "doadmin",
    host: "findmytea-db-do-user-7144332-0.a.db.ondigitalocean.com",
    database: "defaultdb",
    password: process.env.DB_PWD,
    port: 25060,
    ssl: {
      rejectUnauthorized: false,
    },
  });

const spacesEndpoint = new AWS.Endpoint("https://ams3.digitaloceanspaces.com");
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: "2OQGM46UNFGXAXRQWY6I",
  secretAccessKey: process.env.DO_SECRET_KEY,
});

const run = async () => {
  const p = pool();
  const result = await p.query('select "Id", "ImageUrl" FROM public."Brands";');

  var rows = [];

  for (var row of result.rows) {
    rows.push(row);
  }

  p.end();

  for (var r of rows) {
    const url = r.ImageUrl;
    const brandId = r.Id;
    request.get(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        data = Buffer.from(body);

        const newImageId = v4();
        const originFileName = url.substring(url.lastIndexOf("/") + 1);

        var params = {
          Body: data,
          Bucket: "findmytea",
          Key: "images/" + newImageId + "_" + originFileName,
          ContentType: response.headers["content-type"],
          ACL: "public-read",
        };

        s3.putObject(params, async function (err, data) {
          if (err) console.log(err, err.stack);
          else {
            const p = pool();
            await p.query(
              'INSERT INTO public."Images" ("Id", "Path", "OriginalFileName") VALUES(\'' +
                newImageId +
                "', 'images', '" +
                originFileName +
                "')"
            );
            await p.query(
              'INSERT INTO public."BrandImage" ("ImageId", "BrandId") VALUES(\'' +
                newImageId +
                "', '" +
                brandId +
                "')"
            );
            await p.query(
              'UPDATE public."Brands" set "ImageId"=\'' +
                newImageId +
                "' where \"Id\" = '" +
                brandId +
                "'"
            );
            p.end();
          }
        });
      } else {
        console.log(error);
      }
    });
  }
};

run();
